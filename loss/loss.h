#pragma once

#include <util/definitions.h>

double MSELoss(const TDVector& logits, const TDVector& targets);

void MSEGrad(const TDVector& logits, const TDVector& targets, TDVector& grad);

double BCELoss(const TDVector& logits, const TDVector& targets);

void BCEGrad(const TDVector& logits, const TDVector& targets, TDVector& grad);

double AUC(const TDVector& logits, const TDVector& targets);
