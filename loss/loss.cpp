#include "loss.h"

double MSELoss(const TDVector &logits, const TDVector &targets) {
    return (logits - targets).dot(logits - targets) / targets.size();
}

void MSEGrad(const TDVector &logits, const TDVector &targets, TDVector &grad) {
    grad = (logits - targets) * 2;
}

double BCELoss(const TDVector &logits, const TDVector &targets) {
    const TDVector& clippedLogits = logits.unaryExpr([](double x) {return std::min<double>(1 - 1e-7, std::max<double>(x, 1e-7));}).array();
    return -(targets.array() * clippedLogits.array().log() +
             (TDVector::Ones(targets.size()) - targets).array() *
             (TDVector::Ones(logits.size()) - clippedLogits).array().log()).mean();
}

void BCEGrad(const TDVector& logits, const TDVector& targets, TDVector& grad) {
    grad = logits - targets;
}

double AUC(const TDVector& logits, const TDVector& targets) {
    size_t mPlus = 0, mMinus = 0;
    size_t n = logits.size();
    for (size_t i = 0; i < n; ++i) {
        if (targets(i) > 0) {
            ++mPlus;
        } else {
            ++mMinus;
        }
    }
    std::vector<std::pair<float, float>> items;
    for (size_t i = 0; i < n; ++i) {
        items.emplace_back(logits(i), targets(i));
    }
    std::sort(items.begin(), items.end(), std::greater<>());
    double tpr = 0, fpr = 0, auc = 0;
    for (size_t i = 0; i < n; ++i) {
        auto y = items[i].second;
        if (y == 0) {
            fpr += 1.0 / mMinus;
            auc = auc + tpr / mMinus;
        } else {
            tpr += 1.0 / mPlus;
        }
    }
    return auc;
}
