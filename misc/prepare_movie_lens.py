#!/usr/bin/python3

import sys
import numpy
import pandas
from sklearn.model_selection import train_test_split

def load_problem_movielens_100k(dataset_folder, all_features=False):
    '''Standard test dataset for recommendation systems
    From http://grouplens.org/datasets/movielens/
    '''
    folder = dataset_folder
    ratings = pandas.read_csv(folder + '/u.data', sep='\t', 
                              names=['user', 'movie', 'rating', 'timestamp'], header=None)
    ratings = ratings.drop('timestamp', axis=1)
    if all_features:
        users   = pandas.read_csv(folder + '/u.user', sep='|', 
                                  names=['user', 'age', 'gender', 'occupation', 'zip'], header=None)
        movies  = pandas.read_csv(folder + '/u.item', sep='|',
           names=['movie', 'title','released','video_release', 'IMDb URL','unknown','Action','Adventure','Animation',
            'Children','Comedy','Crime','Documentary','Drama','Fantasy','Film-Noir',
            'Horror','Musical','Mystery','Romance','Sci-Fi','Thriller','War','Western'], header=None)
        
        movies = movies.drop(['title', 'IMDb URL', 'video_release'], axis=1)
        movies['released'] = pandas.to_datetime(movies['released']).map(lambda z: z.year)
        ratings = pandas.merge(pandas.merge(ratings, users, on='user'), movies, on='movie')

    answers = ratings['rating'].values
    # ratings = ratings.drop('rating', axis=1)

    for feature in ratings.columns:
        _, ratings[feature] = numpy.unique(ratings[feature], return_inverse=True)
        
    trainX, testX, trainY, testY = train_test_split(ratings, answers, train_size=0.75, random_state=42)
    return trainX, testX, trainY, testY


if __name__ == '__main__':
    folder = sys.argv[1]
    trainX, testX, trainY, testY = load_problem_movielens_100k(folder)
    trainX.to_csv('./temp/train_x.csv', index=False, columns=['rating', 'user', 'movie'], header=False)
    testX.to_csv('./temp/test_x.csv', index=False, columns=['rating', 'user', 'movie'], header=False)