#pragma once

#include <string>


enum class EArgumentType {
    kInt = 0,
    kFloat = 1,
    kString = 2,
    kBool = 3
};

enum class EDataFormat {
    kLibSvm = 0,
    kTsv = 1,
    kCsv = 2,
};

enum class EMode {
    kTrain = 1,
    kEval = 2
};

enum class EModelType {
    kLogReg = 0,
    kFM = 1,
};

enum class EProblemType {
    kClassification = 0,
    kRegression = 1,
};

enum class ELossType {
    kMSE = 1,
    kBCE = 2
};

enum class EOptimizerType {
    kSGD,
    kAdam,
};

struct TConfig {
    EMode Mode;
    std::string TrainFileName;
    std::string TestFileName;

    double LearningRate = 1e-3;

    double LearningRateDecay = 0.95;

    size_t LearningRateDecayPeriod = 1000;

    double DefaultMissingValue = 0.0;

    long ThreadsNumber = 1;

    bool Train;
    EModelType ModelType;
    EProblemType ProblemType;
    EOptimizerType OptimizerType;
    size_t BatchSize;
    std::string ModelCheckpoint;
    size_t NEpochs;
    ELossType Loss;
    size_t EmbeddingSize;
    size_t NNumericalFeatures;
    size_t NCategoricalFeatures;
    size_t NumericalDiscretizationFactor;
    size_t MinFeatureFrequency;
    char FeatureDelimiter;
    std::string Problem;
    size_t CheckpointPeriod;
    std::string OutputFileWithPrediction;
    bool DatasetHasTarget;
    std::string DatasetStateFile;

    // adam optimizer parameters
    double Beta1;
    double Beta2;
};
