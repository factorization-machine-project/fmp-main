#include <cmd_parser.h>
#include <config.h>
#include <job_runner.h>

int main(int argc, const char** argv) {
    TArgumentParser parser;

    parser
        .OptionalArgument("problem", EArgumentType::kString, "criteo")
        .OptionalArgument("problem-type", EArgumentType::kString, "class")
        .OptionalArgument("model-type", EArgumentType::kString, "fm")
        .OptionalArgument("loss", EArgumentType::kString, "bce")
        .OptionalArgument("model-checkpoint", EArgumentType::kString, "model")
        .OptionalArgument("dataset-state-file", EArgumentType::kString)
        .OptionalArgument("batch-size", EArgumentType::kInt, "1024")
        .OptionalArgument("num-lines", EArgumentType::kInt, "0");

    parser.AddSubparser("train")
        .RequiredArgument("train-file", EArgumentType::kString)
        .RequiredArgument("n-num-features", EArgumentType::kInt)
        .RequiredArgument("n-cat-features", EArgumentType::kInt)
        .OptionalArgument("epochs", EArgumentType::kInt, "1")
        .OptionalArgument("embedding-size", EArgumentType::kInt, "16")
        .OptionalArgument("learning-rate", EArgumentType::kFloat, "0.1")
        .OptionalArgument("delimiter", EArgumentType::kString, "\t")
        .OptionalArgument("num-threads", EArgumentType::kInt, "1")
        .OptionalArgument("checkpoint-period", EArgumentType::kInt, "1000")
        .OptionalArgument("discretization-factor", EArgumentType::kInt, "10")
        .OptionalArgument("min-feature-th", EArgumentType::kInt, "1")
        .OptionalArgument("optim", EArgumentType::kString, "sgd")
        .OptionalArgument("beta1", EArgumentType::kFloat, "0.9")
        .OptionalArgument("beta2", EArgumentType::kFloat, "0.999");

    parser.AddSubparser("eval")
        .RequiredArgument("test-file", EArgumentType::kString)
        .OptionalArgument("model-path", EArgumentType::kString, "model")
        .OptionalArgument("output-file", EArgumentType::kString, "preds")
        .Flag("has-target");

    parser.Parse(argc, argv);

    TConfig config;
    config.ModelType = parseModelType(std::any_cast<std::string>(parser.GetArg("model-type")));
    config.Loss = parseLossType(std::any_cast<std::string>(parser.GetArg("loss")));
    config.BatchSize = static_cast<size_t>(std::any_cast<long>(parser.GetArg("batch-size")));
    config.ModelCheckpoint = std::any_cast<std::string>(parser.GetArg("model-checkpoint"));
    config.Problem = std::any_cast<std::string>(parser.GetArg("problem"));
    config.ProblemType = parseProblemType(std::any_cast<std::string>(parser.GetArg("problem-type")));
    config.DatasetStateFile = std::any_cast<std::string>(parser.GetArg("dataset-state-file"));

    if (parser.GetActiveMode() == "train") {
        config.TrainFileName = std::any_cast<std::string>(parser.GetArg("train-file"));
        config.NNumericalFeatures = static_cast<size_t>(std::any_cast<long>(parser.GetArg("n-num-features")));
        config.NCategoricalFeatures = static_cast<size_t>(std::any_cast<long>(parser.GetArg("n-cat-features")));
        config.NumericalDiscretizationFactor = static_cast<size_t>(std::any_cast<long>(parser.GetArg("discretization-factor")));
        config.EmbeddingSize = static_cast<size_t>(std::any_cast<long>(parser.GetArg("embedding-size")));
        config.NEpochs = static_cast<size_t>(std::any_cast<long>(parser.GetArg("epochs")));
        config.MinFeatureFrequency = static_cast<size_t>(std::any_cast<long>(parser.GetArg("min-feature-th")));
        config.FeatureDelimiter = std::any_cast<std::string>(parser.GetArg("delimiter"))[0];
        config.CheckpointPeriod = std::any_cast<long>(parser.GetArg("checkpoint-period"));
        config.LearningRate = std::any_cast<double>(parser.GetArg("learning-rate"));
        config.ThreadsNumber = std::any_cast<long>(parser.GetArg("num-threads"));
        config.OptimizerType = parseOptimizerType(std::any_cast<std::string>(parser.GetArg("optim")));
        config.Beta1 = std::any_cast<double>(parser.GetArg("beta1"));
        config.Beta2 = std::any_cast<double>(parser.GetArg("beta2"));

        config.Mode = EMode::kTrain;
    } else if (parser.GetActiveMode() == "eval") {
        config.TestFileName = std::any_cast<std::string>(parser.GetArg("test-file"));
        config.OutputFileWithPrediction = std::any_cast<std::string>(parser.GetArg("output-file"));
        config.ModelCheckpoint = std::any_cast<std::string>(parser.GetArg("model-path"));
        config.DatasetHasTarget = std::any_cast<bool>(parser.GetArg("has-target"));
        config.Mode = EMode::kEval;
    }

    Eigen::initParallel();
    TJobRunner jobRunner;
    jobRunner.Do(config);
    return 0;
}


//#include <external/benchmark/include/benchmark/benchmark.h>
//
//static void BM_VectorInsert(benchmark::State &state) {
//
//    while (state.KeepRunning()) {
//        std::vector<int> insertion_test;
//        for (int i = 0, i_end = state.range(0); i < i_end; i++) {
//            insertion_test.push_back(i);
//        }
//    }
//}
//
//// Register the function as a benchmark
//BENCHMARK(BM_VectorInsert)->Range(8, 8 << 10);
//
////~~~~~~~~~~~~~~~~
//
//// Define another benchmark
//static void BM_SetInsert(benchmark::State &state) {
//
//    while (state.KeepRunning()) {
//        std::set<int> insertion_test;
//        for (int i = 0, i_end = state.range(0); i < i_end; i++) {
//            insertion_test.insert(i);
//        }
//    }
//}
//BENCHMARK(BM_SetInsert)->Range(8, 8 << 10);
//
//BENCHMARK_MAIN();