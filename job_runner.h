#pragma once

#include <config.h>
#include <dataset/eigen_dataset.h>
#include <models/fm2.h>
#include <optimizers/sgd.h>
#include <optimizers/adam.h>

class TJobRunner {
public:
    void Do(const TConfig& config) {
        if (config.Mode == EMode::kTrain) {
            Train(config);
        }
        if (config.Mode == EMode::kEval) {
            Eval(config);
        }
    }

private:
    void Train(const TConfig& config) {
        std::error_code error;
        std::filesystem::create_directory(config.ModelCheckpoint, error);
        if (error) {
            Y_FAIL("unable to create directory for model saving: " + error.message());
        }

        std::shared_ptr<TCSVDataset> dataset;
        if (!config.DatasetStateFile.empty()) {
            dataset = std::make_shared<TCSVDataset>(config.TrainFileName, config.BatchSize, config.DatasetStateFile, config.FeatureDelimiter);
        } else {
            dataset = std::make_shared<TCSVDataset>(
                    config.TrainFileName, config.BatchSize, config.NNumericalFeatures, config.NCategoricalFeatures,
                    config.NumericalDiscretizationFactor, config.MinFeatureFrequency, config.FeatureDelimiter);
        }
        dataset->SaveState(config.ModelCheckpoint + "/dataset_state");

        NFeatures = dataset->Size().second;

        std::shared_ptr<IOptimizer> optimizer;
        switch (config.OptimizerType) {
            case EOptimizerType::kSGD: {
                optimizer = std::make_shared<TSGDOptimizer>(config.LearningRate);
                break;
            }
            case EOptimizerType::kAdam: {
                optimizer = std::make_shared<TAdamOptimizer>(NFeatures, config.EmbeddingSize, config.LearningRate, config.Beta1, config.Beta2);
                break;
            }
            default: {
                Y_FAIL("Unhandled optimizer type");
            }
        }

        TFM model(config.ProblemType, NFeatures, config.EmbeddingSize, config.ThreadsNumber);

        std::cout << std::endl;
        std::cout << "Training...\n";
        model.Train(dataset, optimizer, config.NEpochs, config.BatchSize, config.CheckpointPeriod, config.ModelCheckpoint);

        std::cout << "Saving model...\n";
        model.Save(config.ModelCheckpoint);
        std::cout << "Model saved at " << std::filesystem::absolute(config.ModelCheckpoint) << std::endl;
    }

    void Eval(const TConfig& config) {
        TFM model(config.ProblemType, config.ModelCheckpoint);
        model.SetDatasetFile(config.TestFileName);
        model.DatasetHasTarget(config.DatasetHasTarget);

        TDVector predictions;
        model.Predict(predictions, config.BatchSize);

        std::ofstream out(config.OutputFileWithPrediction);
        for (size_t i = 0; i < predictions.size(); ++i) {
            out << predictions(i) << '\n';
        }

        std::cout << "Predictions saved at: " << std::filesystem::absolute(config.OutputFileWithPrediction) << std::endl;
    }

private:
    size_t NFeatures = 0;
};
