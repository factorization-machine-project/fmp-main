#include <models/fm2.h>
#include <optimizers/sgd.h>
#include <benchmark/benchmark.h>
#include <set>
#include <vector>


static void BM_FM_Criteo_Small(benchmark::State &state) {
    size_t embedDim = state.range(0);
    size_t nThreads = state.range(1);
    size_t epochs = state.range(2);
    size_t batchSize = state.range(3);
    double lr = 0.05;

    std::cout.setstate(std::ios_base::failbit);
    while (state.KeepRunning()) {
        auto dataset = std::make_shared<TCSVDataset>("datasets/criteo/train_small.txt", '\t', batchSize, 13, 26);
        TFM model(EProblemType::kClassification, dataset->Size().second, embedDim, nThreads);
        std::shared_ptr<IOptimizer> optimizer = std::make_shared<TSGDOptimizer>(lr);
        model.Train(dataset, optimizer, epochs, batchSize, lr);
    }
    std::cout.clear();

}


BENCHMARK(BM_FM_Criteo_Small)
        ->Unit(benchmark::kMillisecond)
        ->Args({16, 1, 5, 128})
        ->Args({16, 2, 5, 128})
        ->Args({16, 4, 5, 128})
        ->Args({16, 8, 5, 128})
        ->Args({16, 1, 5, 256})
        ->Args({16, 2, 5, 256})
        ->Args({16, 4, 5, 256})
        ->Args({16, 8, 5, 256})
        ->Args({50, 1, 5, 128})
        ->Args({50, 2, 5, 128})
        ->Args({50, 4, 5, 128})
        ->Args({50, 8, 5, 128})
        ->Args({50, 1, 5, 256})
        ->Args({50, 2, 5, 256})
        ->Args({50, 4, 5, 256})
        ->Args({50, 8, 5, 256});
