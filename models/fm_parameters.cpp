#include "fm_parameters.h"
#include <util/serialization.h>
#include <mutex>


std::pair<double, bool> TFMParameters::GetW0(bool weak) const {
    return std::make_pair(w0, true);
}

std::pair<double, bool> TFMParameters::GetW(size_t i, bool weak) const {
    return std::make_pair(w(i), true);
}

std::pair<double, bool> TFMParameters::GetV(size_t i, size_t j, bool weak) const {
    return std::make_pair(V(i, j), true);
}

bool TFMParameters::SetW0(double value, bool weak) {
    w0 = value;
    return true;
}

bool TFMParameters::SetW(size_t i, double value, bool weak) {
    w(i) = value;
    return true;
}

bool TFMParameters::SetV(size_t i, size_t j, double value, bool weak) {
    V(i, j) = value;
    return true;
}

void TFMParameters::Save(const std::string& dirname) const {
    {
        auto biasFile = dirname + "/bias";
        std::fstream fout(biasFile, std::ios::out);
        fout << w0;
    }
    {
        auto vecFile = dirname + "/vector";
        SerializeVector(vecFile.data(), w);
    }
    {
        auto embeddingMatrixFile = dirname + "/matrix";
        SerializeDense(embeddingMatrixFile.data(), V);
    }
}

void TFMParameters::Load(const std::string& dirname) {
    {
        auto biasFile = dirname + "/bias";
        std::fstream fin(biasFile);
        fin >> w0;
    }
    {
        auto vecFile = dirname + "/vector";
        DeserializeVector(vecFile.data(), w);
    }
    {
        auto embeddingMatrixFile = dirname + "/matrix";
        DeserializeDense(embeddingMatrixFile.data(), V);
    }

    RowOrder.resize(w.size());
    ColumnOrder.resize(V.cols());
    for (size_t i = 0; i < RowOrder.size(); ++i) {
        RowOrder[i] = i;
    }
    for (size_t i = 0; i < ColumnOrder.size(); ++i) {
        ColumnOrder[i] = i;
    }
}

void TFMParameters::Reset() {
    std::normal_distribution<> distr{0,0.1};
    w0 = distr(Gen);

    // DenseBase::Random is not thread-safe
    for (const auto& i : RowOrder) {
        w(i) = distr(Gen);
    }
    for (const auto& i : RowOrder) {
        for (const auto& j : ColumnOrder) {
            V(i, j) = distr(Gen);
        }
    }
}

void TFMParameters::LoadFrom(const TFMParameters& params, size_t attempts) {
    std::shuffle(RowOrder.begin(), RowOrder.end(), Gen);
    std::shuffle(ColumnOrder.begin(), ColumnOrder.end(), Gen);

    if (attempts == 0) {
        for (const auto &i : RowOrder) {
            for (const auto &j : ColumnOrder) {
                V(i, j) = params.GetV(i, j, false).first;
            }
        }

        w0 = params.GetW0(false).first;

        std::shuffle(RowOrder.begin(), RowOrder.end(), Gen);
        for (const auto &i : RowOrder) {
            w(i) = params.GetW(i, false).first;
        }
    }

    for (size_t attempt = 0; attempt < attempts; ++attempt) {
        for (const auto &i : RowOrder) {
            for (const auto &j : ColumnOrder) {
                auto [val, got] = params.GetV(i, j, true);
                if (got) {
                    V(i, j) = val;
                }
            }
        }

        auto [val, got] = params.GetW0(true);
        if (got) {
            w0 = val;
        }

        for (const auto &i : RowOrder) {
            auto [val, got] = params.GetW(i, true);
            if (got) {
                w(i) = val;
            }
        }
    }
}

void TFMParameters::LoadTo(TFMParameters& params, size_t attempts) const {
    std::shuffle(RowOrder.begin(), RowOrder.end(), Gen);
    std::shuffle(ColumnOrder.begin(), ColumnOrder.end(), Gen);

    if (attempts == 0) {
        params.SetW0(w0, false);

        for (const auto &i : RowOrder) {
            for (const auto &j : ColumnOrder) {
                params.SetV(i, j, V(i, j), false);
            }
        }

        std::shuffle(RowOrder.begin(), RowOrder.end(), Gen);
        for (const auto &i : RowOrder) {
            params.SetW(i, w(i), false);
        }
    }

    for (size_t attempt = 0; attempt < attempts; ++attempt) {
        params.SetW0(w0, true);

        for (const auto &i : RowOrder) {
            for (const auto &j : ColumnOrder) {
                params.SetV(i, j, V(i, j), true);
            }
        }

        std::shuffle(RowOrder.begin(), RowOrder.end(), Gen);
        for (const auto &i : RowOrder) {
            params.SetW(i, w(i), true);
        }
    }
}

std::pair<double, bool> TConcurrentFMParameters::GetW0(bool weak) const {
    if (weak) {
        std::unique_lock<TSpinLock> lock(w0Lock, std::try_to_lock);
        return std::make_pair(lock ? w0 : 0.L, lock.owns_lock());
    }
    std::lock_guard<TSpinLock> lock(w0Lock);
    return std::make_pair(w0, true);
}

std::pair<double, bool> TConcurrentFMParameters::GetW(size_t i, bool weak) const {
    if (weak) {
        std::unique_lock<TSpinLock> lock(wLock[i], std::try_to_lock);
        return std::make_pair(lock ? w(i) : 0.L, lock.owns_lock());
    }
    std::lock_guard<TSpinLock> lock(wLock[i]);
    return std::make_pair(w(i), true);
}

std::pair<double, bool> TConcurrentFMParameters::GetV(size_t i, size_t j, bool weak) const {
    if (weak) {
        std::unique_lock<TSpinLock> lock(VLock[i][j], std::try_to_lock);
        return std::make_pair(lock ? V(i, j) : 0.L, lock.owns_lock());
    }
    std::lock_guard<TSpinLock> lock(VLock[i][j]);
    return std::make_pair(V(i, j), true);
}

bool TConcurrentFMParameters::SetW0(double value, bool weak) {
    if (weak) {
        std::unique_lock<TSpinLock> lock(w0Lock, std::try_to_lock);
        if (lock) {
            w0 = value;
        }
        return lock.owns_lock();
    }
    std::lock_guard<TSpinLock> lock(w0Lock);
    w0 = value;
    return true;
}

bool TConcurrentFMParameters::SetW(size_t i, double value, bool weak) {
    if (weak) {
        std::unique_lock<TSpinLock> lock(wLock[i], std::try_to_lock);
        if (lock) {
            w(i) = value;
        }
        return lock.owns_lock();
    }
    std::lock_guard<TSpinLock> lock(wLock[i]);
    w(i) = value;
    return true;
}

bool TConcurrentFMParameters::SetV(size_t i, size_t j, double value, bool weak) {
    if (weak) {
        std::unique_lock<TSpinLock> lock(VLock[i][j], std::try_to_lock);
        if (lock) {
            V(i, j) = value;
        }
        return lock.owns_lock();
    }
    std::lock_guard<TSpinLock> lock(VLock[i][j]);
    V(i, j) = value;
    return true;
}

void TConcurrentFMParameters::Save(const std::string& dirname) const {
    std::lock_guard<TSpinLock> lock(w0Lock);
    for (const auto& i : RowOrder) {
        for (const auto& j : ColumnOrder) {
            VLock[i][j].lock();
        }
    }
    for (const auto& i : RowOrder) {
        wLock[i].lock();
    }

    TFMParameters::Save(dirname);

    for (const auto& i : RowOrder) {
        wLock[i].unlock();
    }
    for (const auto& i : RowOrder) {
        for (const auto& j : ColumnOrder) {
            VLock[i][j].unlock();
        }
    }
}