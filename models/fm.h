#include <iostream>
#include <reader/reader.h>
#include <cmd_parser.h>
#include <config.h>
#include <functional>

#include <Eigen/Dense>
#include <Eigen/Sparse>

using Eigen::SparseVector;
using Eigen::SparseMatrix;
using Eigen::MatrixXd;
using Eigen::VectorXd;
using Eigen::VectorXi;

class FactorizationMachine {
public:
    FactorizationMachine(size_t nFeatures, size_t k)
            : V(MatrixXd::Random(nFeatures, k))
            , w(VectorXd::Random(nFeatures))
            , w0(0)
    {
    }

    void Forward(const SparseVector<double, RowMajor>& x) const {
        VectorXd embeddingsSum = VectorXd::Zero(V.cols());
        VectorXd embeddingsSquaredSum = VectorXd::Zero(V.cols());
        VectorXd linearPart = VectorXd::Zero(V.cols());
        auto square = [] (double x) {
            return x * x;
        };
        for (Eigen::SparseVector<double, Eigen::RowMajor>::InnerIterator it(sample, 0); it; ++it) {
            size_t featureInd = it.col();
            embeddingsSum += V.row(featureInd);
            embeddingsSquaredSum += V.row(featureInd).unaryExpr(square);
            linearPart += V.row(featureInd) * w(featureInd);
        }

        embeddingsSum = embeddingsSum.unaryExpr(square);

        VectorXd factorizationPart = (embeddingsSum - embeddingsSquaredSum) * 0.5;

        double result = linearPart.sum() + factorizationPart.sum();

        return result;
    }

private:
    MatrixXd V;
    VectorXd w;
    double w0;
};
