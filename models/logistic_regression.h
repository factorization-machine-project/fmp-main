#pragma once

#include <cmath>
#include <random>
#include <unordered_map>
#include <fstream>
#include <iostream>

#include <models/model.h>

class TLogisticRegression : public IModel {
public:
    TLogisticRegression(size_t nDim, double learningRate, double learningRateDecay = 1.0, size_t learningRateDecayPeriod = 1)
        : W(nDim)
        , LearningRate(learningRate)
        , LearningRateDecay(learningRateDecay)
        , LearningRateDecayPeriod(learningRateDecayPeriod)
        , CurrentStep(0)
    {
        std::random_device rd{};
        std::mt19937 gen{rd()};
        std::normal_distribution<> distr{0,1};
        for (size_t i = 0; i < nDim; ++i) {
            W[i] = distr(gen);
        }
    }

    void Train(std::shared_ptr<IDataset> dataset) override {
        for (; ++CurrentStep <= 10000 && !dataset->Empty(); dataset->Next()) {
            const auto& batch = dataset->GetBatch();
            const auto& logits = GetLogits(batch);

            double loss = GetLoss(logits, batch);

            if (CurrentStep % 1000 == 0) {
                std::cout << "Iteration " << CurrentStep << " Loss " << loss << "\n";
            }

            const auto& gradients = GetGradients(batch, logits);

            ApplyGradients(gradients);
        }
    }

    std::vector<double> PredictProba(const TBatch& batch) override {
        return GetLogits(batch);
    }

    std::vector<double> Predict(const TBatch& batch) override {
        auto logits = GetLogits(batch);
        for (auto& logit : logits) {
            logit = std::round(logit);
        }
        return logits;
    }

    void ResetParameters(size_t nDim, double learningRate, double learningRateDecay = 1.0, size_t learningRateDecayPeriod = 1) {
        W.resize(nDim);

        LearningRate = learningRate;
        LearningRateDecay = learningRateDecay;
        LearningRateDecayPeriod = learningRateDecayPeriod;
        CurrentStep = 0;

        std::random_device rd{};
        std::mt19937 gen{rd()};
        std::normal_distribution<> distr{0, 1};
        for (size_t i = 0; i < nDim; ++i) {
            W[i] = distr(gen);
        }
    }

    void Serialize(const std::string& file) override {
        std::ofstream out(file, std::ios::binary | std::ofstream::out);
        out << W.size();
        for (auto& weight : W) {
            out << weight;
        }
        DebugLog("Serialized:", 5);
        out.close();
    }

    void Deserialize(const std::string& file) override {
        std::ifstream in(file, std::ios::binary | std::ofstream::in);
        size_t nDim;
        in >> nDim;
        W.resize(nDim);
        for (size_t i = 0; i < nDim; ++i) {
            in >> W[i];
        }
        DebugLog("Deserialized:", 5);
    }

private:

    double Sigm(double x) const {
        return 1.0 / (1.0 + std::exp(-x));
    }

    double GetLogit(const TSample& sample) const {
        double res = 0.0;
        for (auto [index, value] : sample.Features) {
            res += W[index] * value;
        }
        return Sigm(res);
    }

    std::vector<double> GetLogits(const TBatch& samples) const {
        std::vector<double> logits;
        logits.reserve(samples.size());
        for (const auto& sample : samples) {
            logits.push_back(GetLogit(sample));
        }
        return logits;
    }

    double GetLoss(const std::vector<double>& logits, const TBatch& samples) const {
        assert(logits.size() == samples.size());

        double lossTotal = 0.0;

        for (size_t i = 0; i < logits.size(); ++i) {
            lossTotal += std::log(std::max(logits[i], 1e-9)) * samples[i].Label + std::log(std::max(1 - logits[i], 1e-9)) * (1 - samples[i].Label);
        }
        return -lossTotal / logits.size();
    }

    std::unordered_map<TIndex, double> GetGradients(const TBatch& samples, const std::vector<double>& logits) const {
        size_t n = samples.size();

        assert(n == logits.size());

        std::unordered_map<TIndex, double> gradients;

        for (size_t i = 0; i < n; ++i) {
            double diff = logits[i] - samples[i].Label;
            const auto& sample = samples[i];
            for (auto [featureIndex, featureValue] : sample.Features) {
                gradients[featureIndex] += diff * featureValue;
            }
        }
        for (auto& [index, grad] : gradients) {
            grad /= n;
        }
        return gradients;
    }

    void ApplyGradients(const std::unordered_map<TIndex, double>& gradients) {
        if (CurrentStep % LearningRateDecayPeriod == 0) {
            LearningRate *= LearningRateDecay;
        }
        for (auto& [index, grad] : gradients) {
            W[index] -= grad * LearningRate;
        }
    }

    void DebugLog(const std::string& title, size_t size) const {
        std::cout << title << " ";
        for (size_t i = 0; i < size; ++i) {
            std::cout << W[i] << " ";
        }
        std::cout << std::endl;
    }

    std::vector<double> W;
    double LearningRate;
    double LearningRateDecay;
    size_t LearningRateDecayPeriod;
    size_t CurrentStep;
};
