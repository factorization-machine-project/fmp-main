#pragma once

#include <util/definitions.h>
#include <util/spinlock.h>
#include <random>
#include <deque>

class TFMParameters {
public:
    TFMParameters() = default;

    TFMParameters(size_t nFeatures, size_t nFactors)
        : N(nFeatures)
        , K(nFactors)
        , w(nFeatures)
        , V(nFeatures, nFactors)
        , RowOrder(nFeatures)
        , ColumnOrder(nFactors) {
        for (size_t i = 0; i < nFeatures; ++i) {
            RowOrder[i] = i;
        }
        for (size_t i = 0; i < nFactors; ++i) {
            ColumnOrder[i] = i;
        }
    }

    virtual std::pair<double, bool> GetW0(bool weak) const;
    virtual std::pair<double, bool> GetW(size_t i, bool weak) const;
    virtual std::pair<double, bool> GetV(size_t i, size_t j, bool weak) const;

    virtual bool SetW0(double value, bool weak);
    virtual bool SetW(size_t i, double value, bool weak);
    virtual bool SetV(size_t i, size_t j, double value, bool weak);

    virtual void Save(const std::string& dirname) const;

    void Load(const std::string& dirname);

    void Reset();

    void LoadFrom(const TFMParameters& params, size_t attempts = 0);

    void LoadTo(TFMParameters& params, size_t attempts = 0) const;

    friend class TFM;

protected:
    double w0 = 0.L;
    TDVector w;
    TDMatrix V;

    size_t N = 0;
    size_t K = 0;

    std::random_device Rd{};
    mutable std::mt19937 Gen{Rd()};
    mutable std::vector<size_t> RowOrder;
    mutable std::vector<size_t> ColumnOrder;
};

class TConcurrentFMParameters : public TFMParameters {
public:
    TConcurrentFMParameters(size_t nFeatures, size_t nFactors)
        : TFMParameters(nFeatures, nFactors)
        , wLock(nFeatures)
        , VLock(nFeatures) {
        for (size_t i = 0; i < nFeatures; ++i) {
            VLock[i].resize(nFactors);
        }
    }

    std::pair<double, bool> GetW0(bool weak) const override;
    std::pair<double, bool> GetW(size_t i, bool weak) const override;
    std::pair<double, bool> GetV(size_t i, size_t j, bool weak) const override;

    bool SetW0(double value, bool weak) override;
    bool SetW(size_t i, double value, bool weak) override;
    bool SetV(size_t i, size_t j, double value, bool weak) override;

    void Save(const std::string& dirname) const override;

private:
    mutable TSpinLock w0Lock;
    mutable std::deque<TSpinLock> wLock;
    mutable std::deque<std::deque<TSpinLock>> VLock;
};