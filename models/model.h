#pragma once

#include <config.h>
#include <dataset/dataset.h>
#include <string>

class IModel {
public:
    virtual void Train(std::shared_ptr<IDataset> dataset) = 0;

    virtual std::vector<double> PredictProba(const TBatch& batch) = 0;
    virtual std::vector<double> Predict(const TBatch& batch) = 0;

    virtual void Serialize(const std::string& file) = 0;
    virtual void Deserialize(const std::string& file) = 0;
};
