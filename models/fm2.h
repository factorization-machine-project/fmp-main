#pragma once

#include <cmath>
#include <mutex>
#include <random>
#include <thread>
#include <unordered_map>

#include <config.h>
#include <models/model.h>
#include <loss/loss.h>
#include <util/ctpl_stl.h>
#include <util/serialization.h>
#include <util/definitions.h>

#include <optimizers/optimizer.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <dataset/eigen_dataset.h>
#include <dataset/csv_dataset.h>
#include <filesystem>
#include "fm_parameters.h"

class TFM {
public:
    TFM(EProblemType problemType, const std::string& modelPath)
        : ProblemType(problemType)
        , CheckpointPath(modelPath) {
        Params = std::make_unique<TFMParameters>();
        Dataset = std::make_shared<TCSVDataset>();
        Load(modelPath);
    }

    TFM(EProblemType problemType, size_t nFeatures, size_t nFactors, long numThreads = 1)
        : NFeatures(nFeatures)
        , NFactors(nFactors)
        , ProblemType(problemType)
        , NumThreads(numThreads < 0 ? std::thread::hardware_concurrency() : numThreads) {
        switch (problemType) {
            case EProblemType::kRegression: {
                LossFn = &MSELoss;
                break;
            }
            case EProblemType::kClassification: {
                LossFn = &BCELoss;
                break;
            }
            default: {
                Y_FAIL("Unhandled problem type");
            }
        }

        if (NumThreads == 1) {
            Params = std::make_unique<TFMParameters>(nFeatures, nFactors);
        } else {
            Params = std::make_unique<TConcurrentFMParameters>(nFeatures, nFactors);
        }

        Params->Reset();
    }

    void SetDatasetFile(const std::string& filename) {
        Dataset->SetFile(filename);
    }

    void DatasetHasTarget(bool has) {
        Dataset->HasTarget(has);
    }

    void Train(std::shared_ptr<TCSVDataset>& dataset, std::shared_ptr<IOptimizer>& optimizer, size_t nEpochs = 3, size_t batchSize = 1024,
            size_t savePeriod = 0, std::string checkpointPath = "model") {
        Dataset = dataset;
        Dataset->SetBatchSize(batchSize);
        Optimizer = optimizer;
        CheckpointPath = std::move(checkpointPath);
        SavePeriod = savePeriod;
        const size_t batchPerEpoch = (Dataset->Size().first + batchSize - 1) / batchSize;

        if (NumThreads == 1) {
            Train(nEpochs, batchPerEpoch);
        } else {
            Workers.clear();
            EpochStart = std::chrono::steady_clock::now();
            for (size_t i = 0; i < NumThreads; ++i) {
                Workers.emplace_back([this, nEpochs, batchPerEpoch]() {
                    WorkTrain(nEpochs, batchPerEpoch);
                });
            }
            for (auto &worker : Workers) {
                worker.join();
            }
        }
        std::cout << std::endl;
    }

    void Predict(const TSMatrix& samples, TDVector& predictions) {
        GetPrediction(ProblemType, *Params, samples, predictions);
    }

    void Predict(TDVector& predictions, size_t batchSize = 1024) {
        Dataset->SetBatchSize(batchSize);
        const size_t batchCount = (Dataset->Size().first + batchSize - 1) / batchSize;
        predictions.resize(Dataset->Size().first);

        auto start = std::chrono::steady_clock::now();
        for (size_t batchInd = 0; batchInd < batchCount; ++batchInd) {
            TEigenBatch batch;
            Dataset->GetBatch(batch);

            auto& samples = batch.first;
            TDVector currentPredictions;
            Predict(samples, currentPredictions);
            size_t blockSize = std::min(batchSize, Dataset->Size().first - batchInd * batchSize);
            predictions.middleRows(batchInd * batchSize, blockSize) = currentPredictions.topRows(blockSize);
            DrawProgress(batchInd, batchCount, start);
        }
        std::cout << std::endl;
    }

    void Save(const std::string& dirname) const {
        Params->Save(dirname);
        Dataset->SaveState(dirname + "/dataset_state");
    }

    void Load(const std::string& dirname) {
        Params->Load(dirname);
        Dataset->LoadState(dirname + "/dataset_state");
    }

private:
    void Train(size_t nEpochs, size_t batchPerEpoch) {
        for (; EpochNo < nEpochs; ++EpochNo) {
            EpochStart = std::chrono::steady_clock::now();
            for (BatchesProcessed = 0; BatchesProcessed < batchPerEpoch; ++BatchesProcessed) {
                TEigenBatch batch;
                Dataset->GetBatch(batch);
                auto& [samples, targets] = batch;

                TrainStep(samples, targets);

                if (BatchesProcessed % (batchPerEpoch / 100 + 1) == 0 || (BatchesProcessed + 1) % batchPerEpoch == 0) {
                    TDVector logits;
                    GetPrediction(ProblemType, *Params, samples, logits);
                    auto loss = LossFn(logits, targets);
                    auto score = AUC(logits, targets);
                    DrawProgress(BatchesProcessed, batchPerEpoch, EpochStart, true, EpochNo, nEpochs, loss, score);
                }

                if (SavePeriod != 0 && (BatchesProcessed + 1) % SavePeriod == 0) {
                    Save(CheckpointPath);
                    std::cout << "\033[2K\r" << std::flush << "Checkpoint! Model saved at " << std::filesystem::absolute(CheckpointPath)
                              << " after " << BatchesProcessed + 1 << " iterations" << std::endl;
                }
            }
            std::cout << std::endl;
        }
    }

    void TrainStep(const TSMatrix& samples, const TDVector& targets) {
        TDVector logits;
        GetPrediction(ProblemType, *Params, samples, logits);

        double gradW0;
        TDVector gradW;
        TDMatrix gradV;
        GetGradients(ProblemType, *Params, samples, targets, logits, gradW0, gradW, gradV);
        Optimizer->Update(gradW0, gradW, gradV, Params->w0, Params->w, Params->V);
    }

    void WorkTrain(size_t nEpochs, size_t batchPerEpoch) {
        size_t iterCount = nEpochs * batchPerEpoch;

        TFMParameters params(NFeatures, NFactors);
        TEigenBatch batch;

        for (size_t batchInd = 0;;) {
            std::unique_lock<std::mutex> lock(Mutex);
            Dataset->GetBatch(batch);
            batchInd = BatchesProcessed++;
            lock.unlock();

            if (batchInd >= iterCount) {
                return;
            }

            params.LoadFrom(*Params);

            auto& [samples, targets] = batch;
            TDVector logits;
            GetPrediction(ProblemType, params, samples, logits);

            double gradW0;
            TDVector gradW;
            TDMatrix gradV;
            GetGradients(ProblemType, params, samples, targets, logits, gradW0, gradW, gradV);

            Optimizer->Update(gradW0, gradW, gradV, params.w0, params.w, params.V);
            params.LoadTo(*Params);

            if (SavePeriod != 0 && (batchInd + 1) % SavePeriod == 0) {
                lock.lock();
                Save(CheckpointPath);
                std::cout << "\033[2K\r" << std::flush << "Checkpoint! Model saved at " << std::filesystem::absolute(CheckpointPath)
                          << " after " << batchInd + 1 << " iterations" << std::endl;
                auto loss = LossFn(logits, targets);
                auto score = AUC(logits, targets);
                DrawProgress(batchInd, batchPerEpoch, EpochStart, true, EpochNo, nEpochs, loss, score);
                lock.unlock();
            }

            if (batchInd % (batchPerEpoch / 100 + 1) == 0 || (batchInd + 1) % batchPerEpoch == 0) {
                lock.lock();
                auto loss = LossFn(logits, targets);
                auto score = AUC(logits, targets);
                DrawProgress(batchInd, batchPerEpoch, EpochStart, true, EpochNo, nEpochs, loss, score);
                if ((batchInd + 1) % batchPerEpoch == 0) {
                    std::cout << std::endl;
                    EpochStart = std::chrono::steady_clock::now();
                    ++EpochNo;
                }
            }
        }
    }

    static void GetPrediction(EProblemType problemType, const TFMParameters& params, const TSMatrix& X, TDVector& predictions) {
        predictions = X * params.w;
        predictions.noalias() += ((X * params.V).cwiseAbs2() - (X.cwiseAbs2() * params.V.cwiseAbs2())).rowwise().sum() / 2;
        predictions = predictions.array() + params.w0;
        if (problemType == EProblemType::kClassification) {
            predictions = (-predictions).array().exp();
            predictions += TDVector::Ones(predictions.size());
            predictions = predictions.array().pow(-1);
        }
    }

    static void GetGradients(EProblemType problemType, const TFMParameters& params,
            const TSMatrix& X, const TDVector& y, const TDVector& pred, double& gradW0, TDVector& gradW, TDMatrix& gradV) {
        size_t n = y.size();

        assert(n == pred.size());

        TDVector grad;
        switch (problemType) {
            case EProblemType::kRegression: {
                MSEGrad(pred, y, grad);
                break;
            }
            case EProblemType::kClassification: {
                BCEGrad(pred, y, grad);
                break;
            }
            default: {
                Y_FAIL("Unhandled problem type");
            }
        }
        gradW0 = grad.sum() / n;

        gradW = X.transpose() * grad / n;
        
        gradV = X.transpose() * (grad.asDiagonal() * (X * params.V));
        gradV.noalias() -= (params.V.array().colwise() * (X.cwiseAbs2().transpose() * grad).array()).matrix();
        gradV /= n;
    }

    static void DrawProgress(size_t batchInd, size_t batchPerEpoch, std::chrono::steady_clock::time_point epochStart,
            bool train = false, size_t epochNo = 0, size_t nEpochs = 0, double loss = 0.L, double score = 0.L) {
        batchInd %= batchPerEpoch;
        int nEqs = batchPerEpoch > 1 ? 99 * batchInd / (batchPerEpoch - 1) : 99;
        auto timePerBatch = batchInd ? (std::chrono::steady_clock::now() - epochStart) / batchInd : std::chrono::seconds{0};
        std::stringstream fTimePerBatch;
        if (timePerBatch > std::chrono::seconds{1}) {
            fTimePerBatch << std::chrono::duration_cast<std::chrono::seconds>(timePerBatch).count() << 's';
        } else {
            fTimePerBatch << std::chrono::duration_cast<std::chrono::milliseconds>(timePerBatch).count() << "ms";
        }
        std::cout << "\033[2K\r" << std::flush;
        if (train) {
            std::cout << "Epoch " << epochNo + 1 << '/' << nEpochs << "  ";
        }
        std::cout << "Batch " << batchInd + 1 << '/' << batchPerEpoch << "  " << fTimePerBatch.str() << "/batch"
                  << " \033[32m[" << std::string(nEqs, '=') << (nEqs == 99 ? "" : ">") << std::string(std::max(98 - nEqs, 0), '-') << "]\033[39m" << std::flush;
        if (train) {
            std::cout << " loss: " << loss << "  AUC: " << score << std::flush;
        }
    };

private:
    std::function<double(const TDVector&, const TDVector&)> LossFn;

    const EProblemType ProblemType;

    size_t NFeatures = 0;
    size_t NFactors = 0;
    std::unique_ptr<TFMParameters> Params{nullptr};
    std::shared_ptr<IOptimizer> Optimizer{nullptr};

    std::mutex Mutex;
    std::shared_ptr<TCSVDataset> Dataset{nullptr};
    size_t BatchesProcessed = 0;

    std::string CheckpointPath = "model";
    size_t SavePeriod = 0;

    const size_t NumThreads = 1;
    std::vector<std::thread> Workers;

    size_t EpochNo = 0;
    std::chrono::steady_clock::time_point EpochStart;
};
