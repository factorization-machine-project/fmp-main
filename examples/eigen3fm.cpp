#include "eigen3fm.h"

#include <iostream>
#include <config.h>
#include <functional>
#include <memory>

#include <cmd_parser.h>
#include <models/fm2.h>
#include <reader/reader.h>

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <reader/movielens_reader.h>

//typedef Eigen::SparseMatrix<double> SMatrix;
//typedef Eigen::SparseVector<double> SVector;
typedef Eigen::MatrixXd DMatrix;
typedef Eigen::VectorXd DVector;


int main_mnist_eigen3_fm() {
    size_t n = 4;
    size_t k = 2;
    double w0 = 0.5;
    DVector w(n);
    w << 1, -1, 2, -2; // init
    DMatrix V(n, k);
    V << 1, 2, 4, 3, -2, 5, 6, -1; // init
    DMatrix V2 = V.cwiseAbs2();

    size_t b = 3;
    TSMatrix X(b, n);
    X.insert(0, 0) = 2;
    X.insert(0, 2) = 1;
    X.insert(1, 1) = 3;
    X.insert(1, 3) = 1;
    X.insert(2, 2) = 3;
    X.insert(2, 3) = 2;
    TSMatrix X2 = X.cwiseAbs2();

    DVector y = w0 + (X * w + ((X * V).cwiseAbs2() - (X2 * V2)).rowwise().sum() / 2).array();
    auto pred = y * 3;

    DVector diff = pred - y;
    double gradW0 = diff.sum() * (2.0 / n);
    DVector gradW = X.transpose() * diff * (2.0 / b);
    DMatrix gradV = (
        X.transpose() * (diff.asDiagonal() * (X * V))
        - (V.array().colwise() * (X.cwiseAbs2().transpose() * diff).array()).matrix()
    ) * (2.0 / b);
    
    std::cout << gradW0 <<"\n\n";
    std::cout << gradW <<"\n\n";
    std::cout << gradV <<"\n\n";

    return 0;
}

int main_movielens_eigen3_fm(int argc, const char** argv) {
    TArgumentParser parser;

    parser.AddArgument("train-file", EArgumentType::kString);
    parser.AddArgument("test-file", EArgumentType::kString);
    parser.AddArgument("learning-rate", EArgumentType::kFloat, false, "0.001");
    parser.AddArgument("learning-rate-decay", EArgumentType::kFloat, false, "0.95");
    parser.AddArgument("learning-rate-decay-period", EArgumentType::kInt, false, "10000");
    parser.AddArgument("default-missing-value", EArgumentType::kFloat, false, "0.0");

    parser.Parse(argc, argv);

    TConfig config{
        .TrainFileName = std::any_cast<std::string>(parser.GetArg("--train-file")),
        .TestFileName = std::any_cast<std::string>(parser.GetArg("--test-file")),
        .LearningRate = std::any_cast<double>(parser.GetArg("--learning-rate")),
        .LearningRateDecay = std::any_cast<double>(parser.GetArg("--learning-rate-decay")),
        .LearningRateDecayPeriod = static_cast<size_t>(std::any_cast<long>(parser.GetArg("--learning-rate-decay-period"))),
        .DefaultMissingValue = std::any_cast<double>(parser.GetArg("--default-missing-value")),
    };

    TMovieLensReader reader(config.TrainFileName);

    auto [X, y] = reader.ReadSparseFeatures();

    TFM model(EProblemType::kRegression, X.cols(), 50, 1);

    // model.Train(X, y, 3, 64, 1e-3);

    return 0;
}
