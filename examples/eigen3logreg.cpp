#include "eigen3logreg.h"

#include <iostream>
#include <reader/reader.h>
#include <cmd_parser.h>
#include <config.h>
#include <functional>
#include <memory>

#include <Eigen/Dense>

using Eigen::MatrixXd;
using Eigen::VectorXd;


int main(int argc, const char** argv) {
    TArgumentParser parser;

    parser.AddArgument("train-file", EArgumentType::kString);
    parser.AddArgument("test-file", EArgumentType::kString);
    parser.AddArgument("learning-rate", EArgumentType::kFloat, false, "0.001");
    parser.AddArgument("learning-rate-decay", EArgumentType::kFloat, false, "0.95");
    parser.AddArgument("learning-rate-decay-period", EArgumentType::kInt, false, "10000");
    parser.AddArgument("default-missing-value", EArgumentType::kFloat, false, "0.0");

    parser.Parse(argc, argv);

    TConfig config{
            .TrainFileName = std::any_cast<std::string>(parser.GetArg("--train-file")),
            .TestFileName = std::any_cast<std::string>(parser.GetArg("--test-file")),
            .LearningRate = std::any_cast<double>(parser.GetArg("--learning-rate")),
            .LearningRateDecay = std::any_cast<double>(parser.GetArg("--learning-rate-decay")),
            .LearningRateDecayPeriod = static_cast<size_t>(std::any_cast<long>(parser.GetArg("--learning-rate-decay-period"))),
            .DefaultMissingValue = std::any_cast<double>(parser.GetArg("--default-missing-value")),
    };

    size_t DIM = 64;

    TSpec spec;
    for (size_t i = 0; i < DIM; ++i) {
        spec.emplace(i, EColumnType::kNumerical);
    }

    std::shared_ptr<TDataReader> reader = std::make_shared<TDataReader>(config.TrainFileName, EProblemType::kClassification, EDataFormat::kTsv, config.DefaultMissingValue, spec);

    std::vector<TSample> samples;

    for (; reader->IsValid(); reader->Next()) {
        samples.push_back(reader->GetRow());
    }

    MatrixXd X(samples.size(), DIM);
    VectorXd Y(samples.size(), 1);

    for (size_t i = 0; i < samples.size(); ++i) {
        for (size_t j = 0; j < samples[i].Features.size(); ++j) {
            X(i, j) = samples[i].Features[j].second;
        }
        Y(i) = samples[i].Label;
    }

    VectorXd W = VectorXd::Random(DIM);

    auto Log = [](double x) {return std::log(std::max(x, 1e-9));};
    auto Sigm = [](double x) {return 1.0 / (1.0 + std::exp(-x));};

    double lr = 1e-3;

    for (size_t iter = 0; iter < 100; ++iter) {
        VectorXd logits = (X * W).unaryExpr(Sigm);

        VectorXd lossV = Y.transpose() * logits.unaryExpr(Log) + ((VectorXd::Ones(Y.size()) - Y).transpose() * ((VectorXd::Ones(Y.size()) - logits).unaryExpr(Log)));
        double loss = -lossV(0);

        VectorXd clippedLogits = logits.unaryExpr([] (double x) {return std::round(x);});

        double acc = ((clippedLogits - Y).unaryExpr([] (double x) {return x == 0.0 ? 1.0 : 0.0;})).sum() / Y.size();

        VectorXd grads = (logits - Y).transpose() * X;

        W -= grads * lr;

        std::cout << "Iter: " << iter + 1 << " loss: " << loss << " accuracy: " << acc << "\n";
    }

    return 0;
}
