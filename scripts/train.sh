#!/usr/bin/env bash
./fmp train \
    --train-file \
    ../data/train.txt \
    --epochs 5 \
    --embedding-size 16 \
    --n-num-features 13 \
    --n-cat-features 26 \
    --min-feature-th 10
