#pragma once
#include <util/definitions.h>

class IOptimizer {
public:
    virtual void Update(const double w0Grad, const TDVector& wGrad, const TDMatrix& VGrad, double& w0, TDVector& w, TDMatrix& V) = 0;
    virtual ~IOptimizer() = default;
};