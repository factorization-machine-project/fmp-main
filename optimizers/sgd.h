#pragma once

class TSGDOptimizer : public IOptimizer {
public:
    TSGDOptimizer(double lr = 0.1, double weightDecay = 0.1)
        : Lr(lr)
        , WeightDecay(weightDecay) {
    }

    void Update(const double w0Grad, const TDVector& wGrad, const TDMatrix& VGrad, double& w0, TDVector& w, TDMatrix& V) override {
        w0 = w0 - Lr * w0Grad - WeightDecay * w0;
        w *= (1 - WeightDecay);
        w.noalias() -= Lr * wGrad;
        V *= (1 - WeightDecay);
        V.noalias() -= Lr * VGrad;
    }

private:
    double Lr;
    double WeightDecay;
};