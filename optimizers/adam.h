#pragma once

#include "optimizer.h"

class TAdamOptimizer : public IOptimizer {
public:
    TAdamOptimizer(size_t NFeatures, size_t NFactors, double lr = 1e-3, double weightDecay = 0., double beta1 = 0.9, double beta2 = 0.999)
        : Lr(lr)
        , WeightDecay(weightDecay)
        , Beta1(beta1)
        , Beta2(beta2) {
        wMu = TDVector::Zero(NFeatures);
        wNu = TDVector::Zero(NFeatures);
        VMu = TDMatrix::Zero(NFeatures, NFactors);
        VNu = TDMatrix::Zero(NFeatures, NFactors);
    }

    void Update(const double w0Grad, const TDVector& wGrad, const TDMatrix& VGrad, double& w0, TDVector& w, TDMatrix& V) override {
        double lr = Lr * std::sqrt(1 - std::pow(Beta2, timestamp)) / (1 - std::pow(Beta1, timestamp));
        double gradW0 = w0Grad + w0 * WeightDecay;

        TDVector gradW = wGrad;
        gradW.noalias() += w * WeightDecay;

        TDMatrix gradV = VGrad;
        gradV.noalias() += V * WeightDecay;

        w0Mu = Beta1 * w0Mu + (1 - Beta1) * gradW0;
        w0Nu = Beta2 * w0Nu + (1 - Beta2) * gradW0 * gradW0;
        w0 -= lr * w0Mu / (std::sqrt(w0Nu) + eps);

        wMu *= Beta1;
        wMu.noalias() += (1 - Beta1) * gradW;
        wNu *= Beta2;
        wNu.noalias() += (1 - Beta2) * (gradW.array() * gradW.array()).matrix();
        w.noalias() -= lr * (wMu.array() / (wNu.array().sqrt() + eps)).matrix();

        VMu *= Beta1;
        VMu.noalias() += (1 - Beta1) * gradV;
        VNu *= Beta2;
        VNu.noalias() += (1 - Beta2) * (gradV.array() * gradV.array()).matrix();
        V.noalias() -= lr * (VMu.array() / (VNu.array().sqrt() + eps)).matrix();

        ++timestamp;
    }

private:
    const double eps = 1e-8;

    double Lr;
    double WeightDecay;

    double Beta1, Beta2;
    double w0Mu = 0, w0Nu = 0;
    TDVector wMu, wNu;
    TDMatrix VMu, VNu;
    size_t timestamp = 1;
};