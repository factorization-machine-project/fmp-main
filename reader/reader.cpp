#include "reader.h"

// parsing spec like 'N13C39'
TSpec parseSpec(std::string str) {
    TSpec spec;
    for (size_t i = 0; i < str.size(); ++i) {
        if (str[i] == 'N' || str[i] == 'C') {
            EColumnType column_type = str[i] == 'N' ? EColumnType::kNumerical : EColumnType::kCategorical;
            size_t cnt = 0;
            while (i < str.size() - 1 && std::isdigit(str[i + 1])) {
                ++i;
                cnt = cnt * 10 + str[i] - '0';
            }
            if (cnt == 0) cnt = 1;
            for (size_t j = 0; j < cnt; ++j) {
                spec.emplace(j, column_type);
            }
        }
    }
    return spec;
}
