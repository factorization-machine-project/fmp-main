#pragma once
#include <reader/reader.h>
#include <config.h>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <array>
#include <string>
#include <unordered_map>

class TCriteoReader {
public:
    TCriteoReader() = default;

    TCriteoReader(const std::string& filename) : reader_(filename, EProblemType::kClassification, EDataFormat::kTsv, 0.0, parseSpec("N13C39")) {
        TDataReader first_pass_reader(filename, EProblemType::kClassification, EDataFormat::kTsv, 0.0, parseSpec("N13C39"));
        int row_idx = 0;
        for (;first_pass_reader.IsValid(); first_pass_reader.Next()) {
            first_pass_reader.GetRow();
            auto categorical_data = first_pass_reader.GetCategoricalFeaturesData();
            for (auto&& key : categorical_data) {
                for (auto&& k : key.second) {
                    if (k.first == "") {
                        continue;
                    }
                    int idx = key.first - kCriteoNumericalFeaturesCount;
                    if (ohe_encodings_[idx].find(k.first) == ohe_encodings_[idx].end()) {
                        ohe_encodings_[idx][k.first] = categorical_feature_values_cnt_[idx];
                        categorical_feature_values_cnt_[idx]++;
                    }
                }
            }
            
            ++row_idx;
            if (row_idx % 1000 == 0) {
                std::cout << "Row " << row_idx << std::endl;
            }
        }

        categorical_feature_starting_idx_[0] = kCriteoNumericalFeaturesCount;
        for (int idx = 0; idx < kCriteoCategoricalFeaturesCount - 1; ++idx) {
            categorical_feature_starting_idx_[idx + 1] = categorical_feature_starting_idx_[idx] + categorical_feature_values_cnt_[idx]; 
        }
    }

    Eigen::SparseVector<double> ReadRatingsRow() {
        size_t cur_cols = categorical_feature_starting_idx_[kCriteoCategoricalFeaturesCount - 1]
                + categorical_feature_values_cnt_[kCriteoCategoricalFeaturesCount - 1];
        Eigen::SparseVector<double, Eigen::RowMajor> vector(1, cur_cols);
        if (reader_.IsValid()) {
            TSample sample = reader_.GetRow();
            for (auto&& key : sample.Features) {
                if (key.first < kCriteoNumericalFeaturesCount) {
                    vector.insert(0, key.first) = 1.0 * key.second;
                }
                
            }

            auto categorical_data = reader_.GetCategoricalFeaturesData();
            for (auto&& key : categorical_data) {
                for (auto&& k : key.second) {
                    if (k.first == "") {
                        continue;
                    }
                    int idx = key.first - kCriteoNumericalFeaturesCount;
                    int feature_idx = ohe_encodings_[idx][k.first] + kCriteoNumericalFeaturesCount;
                    vector.insert(0, feature_idx) = 1;
                    
                }
            }
        }

        return vector;
    }

private:
    constexpr static int kCriteoNumericalFeaturesCount = 13;
    constexpr static int kCriteoCategoricalFeaturesCount = 26;
    TDataReader reader_;
    std::array<int, kCriteoCategoricalFeaturesCount> categorical_feature_starting_idx_{0};
    std::array<int, kCriteoCategoricalFeaturesCount> categorical_feature_values_cnt_{0};
    std::array<std::unordered_map<std::string, int>, kCriteoCategoricalFeaturesCount> ohe_encodings_;
};