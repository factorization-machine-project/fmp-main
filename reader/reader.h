#pragma once

#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <optional>
#include <iostream>
#include <unordered_map>
#include <cassert>

#include <dataset/sample.h>
#include <config.h>

enum class EColumnType {
    kNumerical = 0,
    kCategorical = 1
};

using TSpec = std::unordered_map<size_t, EColumnType>;

TSpec parseSpec(std::string str);

class TDataReader {
public:
    TDataReader() = default;

    TDataReader(const std::string& filename,
                EProblemType problemType,
                EDataFormat dataFormat,
                double defaultMissingValue = 0.0, // we need this to fill up missing values in criteo dataset
                std::optional<TSpec> spec = std::nullopt)
        : InputStream(filename)
        , ProblemType(problemType)
        , DataFormat(dataFormat)
        , DefaultMissingValue(defaultMissingValue)
        , CurrentLine("")
        , ValuePair("")
        , Label("")
        , Sample(std::nullopt)
        , IsCurrentRowProcessed(true)
        , RowIndex(0)
        , Spec(std::move(spec))
        , NNumericalFeatures(0)
        , NCategoricalFeatures(0)
    {
        if (InputStream.fail()) {
            throw std::runtime_error("Couldn't open file" + filename);
        }
        if (Spec.has_value()) {
            for (const auto& [index, columnType] : *Spec) {
                if (columnType == EColumnType::kNumerical) {
                    ++NNumericalFeatures;
                }
            }
        }
        Next();
    }

    TSample GetRow() {
        if (!IsCurrentRowProcessed) {
            switch (DataFormat) {
            case EDataFormat::kLibSvm:
                ProcessCurrentRowLibSvm();
                break;
            case EDataFormat::kTsv:
                ProcessCurrentRowTsv();
                break;
            case EDataFormat::kCsv:
                ProcessCurrentRowCsv();
                break;
            }
        }
        Valid = !InputStream.eof();
        return *Sample;
    }

    std::unordered_map<size_t, std::unordered_map<std::string, TIndex>> GetCategoricalFeaturesData() {
        return OneHotEncodedColumns;
    }

    void Next() {
        if (!IsValid()) {
            return;
        }
        std::getline(InputStream, CurrentLine);
        IsCurrentRowProcessed = false;
        ++RowIndex;
    }

    bool IsValid() const {
        return Valid;
    }

    ~TDataReader() {
        InputStream.close();
    }

    TSpec GetSpec() const {
        assert(Spec.has_value());
        return *Spec;
    }

    TIndex GetNNumericalFeatures() const {
        return NNumericalFeatures;
    }

    TIndex GetNCategoricalFeatures() const {
        return NCategoricalFeatures;
    }

    size_t GetRowIndex() const {
        return RowIndex - 1;
    }

private:
    void ProcessCurrentRowLibSvm() {
        std::stringstream buffer(CurrentLine);

        std::getline(buffer, Label, ' ');

        double label = std::stod(Label);

        if (ProblemType == EProblemType::kClassification) {
            assert(label == 0.0 || label == 1.0);
        }

        TSample sample{
            .Label = label
        };

        while (std::getline(buffer, ValuePair, ' ')) {
            if (ValuePair == "\n") {
                break;
            }
            if (auto it = std::find(ValuePair.begin(), ValuePair.end(), ':'); it != std::end(ValuePair)) {
                TIndex featureInd = std::stol(ValuePair.substr(0, std::ptrdiff_t(it - ValuePair.begin())));
                TValue featureValue = std::stod(ValuePair.substr(it - ValuePair.begin() + 1, ValuePair.end() - it - 1));
                sample.Features.emplace_back(featureInd, featureValue);
            } else {
                throw std::runtime_error("invalid token: " + ValuePair);
            }
        }

        Sample = sample;
        IsCurrentRowProcessed = true;
    }

    void ProcessCurrentRowTsv() {
        ParseCharacterSeparatedLine('\t');
    }

    void ProcessCurrentRowCsv() {
        ParseCharacterSeparatedLine(',');
    }

    void EmplaceCategoricalFeature(TSample& sample, size_t columnIndex, const std::string& featureValue) {
        if (auto catFeatureIndex = OneHotEncodedColumns[columnIndex].find(featureValue); catFeatureIndex != OneHotEncodedColumns[columnIndex].end()) {
            sample.Features.emplace_back(catFeatureIndex->second, 1.0);
        } else {
            OneHotEncodedColumns[columnIndex].emplace(featureValue, NNumericalFeatures + NCategoricalFeatures);
            sample.Features.emplace_back(NNumericalFeatures + NCategoricalFeatures, 1.0);
            ++NCategoricalFeatures;
        }
    }

    void ParseCharacterSeparatedLine(char sep) {
        std::stringstream buffer(CurrentLine);
        std::getline(buffer, Label, sep);
        double label = std::stod(Label);

        if (ProblemType == EProblemType::kClassification) {
            assert(label == 0.0 || label == 1.0);
        }

        TSample sample{
            .Label = label
        };

        if (!Spec.has_value()) {
            size_t featureIndex = 0;
            while (std::getline(buffer, ValuePair, sep)) {
                TValue featureValue = std::stod(ValuePair);
                sample.Features.emplace_back(featureIndex, featureValue);
                ++featureIndex;
            }
        } else {
            size_t featureIndex = 0;
            while (std::getline(buffer, ValuePair, sep)) {
                if (Spec->at(featureIndex) == EColumnType::kNumerical) {
                    TValue featureValue = !ValuePair.empty() ? std::stod(ValuePair) : DefaultMissingValue;
                    sample.Features.emplace_back(featureIndex, featureValue);
                } else { // Categorical
                    EmplaceCategoricalFeature(sample, featureIndex, ValuePair);
                }
                ++featureIndex;
            }

            if (sample.Features.size() + 1 == Spec->size()) {
                EmplaceCategoricalFeature(sample, featureIndex, "");
            }

            assert(sample.Features.size() == Spec->size());
        }

        Sample = sample;
        IsCurrentRowProcessed = true;
    }

    std::ifstream InputStream;
    EProblemType ProblemType;
    EDataFormat DataFormat;
    double DefaultMissingValue;
    std::string CurrentLine;
    std::string ValuePair;
    std::string Label;
    std::optional<TSample> Sample;
    bool IsCurrentRowProcessed;
    bool Valid = true;
    size_t RowIndex;

    std::optional<TSpec> Spec; // contains specification for each column. Supports only tsv data format
    TIndex NNumericalFeatures;
    TIndex NCategoricalFeatures;
    std::unordered_map<size_t, std::unordered_map<std::string, TIndex>> OneHotEncodedColumns;
};
