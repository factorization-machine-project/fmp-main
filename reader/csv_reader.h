#pragma once

#include <fstream>
#include <iostream>
#include <algorithm>
#include <memory>

#include <config.h>
#include <reader/reader.h>
#include <util/definitions.h>


class TCsvReader {
public:
    TCsvReader() = default;

    explicit TCsvReader(size_t nCols, char delim, size_t minFreq = 20)
        : NCols(nCols)
        , Delim(delim)
        , MinFreq(minFreq)
        , UseOldSpec(false)
    {
    }

    void Read(const std::string& filename, TSMatrix& matrix, TDVector& targets, size_t nLinesToRead = 0, ssize_t nPredifinedRows = -1) {
        size_t nRows = 0;

        if (!UseOldSpec) {
            std::vector<std::unordered_map<std::string, size_t>> hash2id(NCols);
            std::vector<std::unordered_map<std::string, size_t>> hash2freq(NCols);
            for (auto& h : hash2id) {
                h.reserve(1u << 20);
            }

            auto accHashes = [&hash2id, &hash2freq] (const std::vector<std::string>& values) -> bool {
                for (size_t i = 0; i < values.size(); ++i) {
                    hash2id[i].emplace(values[i], hash2id[i].size());
                    ++hash2freq[i][values[i]];
                }
                return true;
            };

            nRows = ReadCsv(filename, NCols, Delim, accHashes, nLinesToRead);

            std::vector<size_t> prefixSums{0};

            for (size_t i = 1; i < hash2id.size(); ++i) {
                for (auto it = hash2freq[i].begin(); it != hash2freq[i].end();) {
                    if (it->second < MinFreq) {
                        it = hash2freq[i].erase(it);
                    } else {
                        ++it;
                    }
                }

                size_t ind = 0;
                for (auto& [k, v] : hash2id[i]) {
                    v = ind++;
                }
                prefixSums.push_back(prefixSums.back() + hash2id[i].size());
            }

            Hash2Id = std::move(hash2id);
            Offsets = std::move(prefixSums);
        } else if (nPredifinedRows == -1) {
            nRows = ReadCsv(filename, NCols, Delim, [](const std::vector<std::string>&) { return true; }, nLinesToRead);
        } else {
            nRows = static_cast<size_t>(nPredifinedRows);
        }

        matrix.resize(nRows, Offsets.back());
        targets.resize(nRows);

        size_t rowIndex = 0;

        auto fillRow = [this, &matrix, &targets, &rowIndex] (const std::vector<std::string>& values) -> bool {
            targets(rowIndex) = std::stod(values[0]);
            for (size_t j = 1; j < values.size(); ++j) {
                auto valPtr = Hash2Id[j].find(values[j]);
                if (valPtr == Hash2Id[j].end()) {
                    continue;
                }
                size_t featureIndex = valPtr->second + Offsets[j - 1];
                matrix.insert(rowIndex, featureIndex) = 1;
            }

            ++rowIndex;

            return true;
        };

        ReadCsv(filename, NCols, Delim, fillRow, nLinesToRead);
    }

    void ReuseOldSpec() {
        if (Hash2Id.empty()) {
            Y_FAIL("Can't use old spec because it is empty");
        }
        UseOldSpec = true;
    }

    void UseNewSpec() {
        UseOldSpec = false;
    }

private:
    size_t ReadCsv(const std::string& filename, size_t nCols, char delim, const std::function<bool(const std::vector<std::string>&)>& func, size_t nLinesToRead = 0) const;

    std::vector<std::unordered_map<std::string, size_t>> Hash2Id;
    std::vector<size_t> Offsets;

    size_t NCols;
    char Delim;
    size_t MinFreq;

    bool UseOldSpec;
};

std::shared_ptr<TCsvReader> GetDatasetReader(const std::string& problem);
