#include "csv_reader.h"
#include "csv.h"

#include <vector>
#include <string>
#include <iostream>
#include <functional>

std::shared_ptr<io::ICSVReader> GetReader(const std::string& filename, size_t nCols, char delim = '\t') {
    if (nCols == 40 && delim == '\t') {
        return std::make_shared<io::CSVReader<40, io::trim_chars<'\n', ' '>, io::no_quote_escape<'\t'>>>(filename);
    }
    if (nCols == 3 && delim == ',') {
        return std::make_shared<io::CSVReader<3, io::trim_chars<'\n', ' '>, io::no_quote_escape<','>>>(filename);
    }
    if (nCols == 65 && delim == '\t') {
        return std::make_shared<io::CSVReader<65, io::trim_chars<'\n', ' '>, io::no_quote_escape<'\t'>>>(filename);
    }
    return std::make_shared<io::CSVReader<24, io::trim_chars<'\n', ' '>, io::no_quote_escape<','>>>(filename);
    Y_FAIL("Unhandled reader");
}

size_t TCsvReader::ReadCsv(const std::string& filename, size_t nCols, char delim, const std::function<bool(const std::vector<std::string>&)>& func, size_t nLinesToRead) const {
    auto reader = GetReader(filename, nCols, delim);
    std::vector<std::string> values(nCols);

    size_t cnt = 0;

    while(reader->read_row_vec(values)){
        // do stuff with the data
        ++cnt;
        if (!func(values) || (nLinesToRead != 0 && cnt >= nLinesToRead)) {
            break;
        }
        if (cnt % 1000000 == 0) {
            std::cout << "Progress: " << cnt << "\n";
        }
    }

    std::cout << "It's " << cnt << ". Stop!\n";
    return cnt;
}

std::shared_ptr<TCsvReader> GetDatasetReader(const std::string& problem) {
    if (problem == "criteo") {
        return std::make_shared<TCsvReader>(40, '\t', 20);
    }
    if (problem == "movielens") {
        return std::make_shared<TCsvReader>(3, ',', 1);
    }
    if (problem == "mnist_simple") {
        return std::make_shared<TCsvReader>(65, '\t', 1);
    }
    Y_FAIL("unknown problem type");
}
