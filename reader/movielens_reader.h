#pragma once
#include <reader/reader.h>
#include <config.h>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <Eigen/Dense>
#include <Eigen/Sparse>

class TMovieLensReader {
public:
    TMovieLensReader() = default;

    TMovieLensReader(const std::string& filename) : reader_(filename, EProblemType::kRegression, EDataFormat::kCsv) {
    }

    Eigen::SparseMatrix<double> ReadRatings() {
        Eigen::SparseMatrix<double> matrix;
        size_t cur_rows = 0;
        size_t cur_cols = 0;
        reader_.Next();
        while (reader_.IsValid()) {
            
            TSample sample = reader_.GetRow();
            
            size_t row, col;
            for (auto v : sample.Features) {
                if (v.first == 0) {
                    row = v.second;
                } else if (v.first == 1) {
                    col = v.second;
                }
            }
            cur_rows = std::max(cur_rows, row+1);
            cur_cols = std::max(cur_cols, col+1);
            matrix.resize(cur_rows, cur_cols);

            auto val = matrix.insert(row, col);
            val = sample.Label;

            reader_.Next();
        }

        return matrix;
    }

    std::pair<Eigen::SparseMatrix<double, Eigen::RowMajor>, Eigen::VectorXd> ReadSparseFeatures() {
        std::vector<TSample> samples;

        size_t maxCol = 0;

        std::vector<size_t> spaceSizes;

        for (;reader_.IsValid(); reader_.Next()) {
            samples.push_back(reader_.GetRow());
            size_t index = 0;
            for (const auto& f : samples.back().Features) {
                if (index == spaceSizes.size()) {
                    spaceSizes.emplace_back(); // 0
                }
                spaceSizes[index] = std::max<size_t>(spaceSizes[index], f.second + 1);
                ++index;
            }
        }

        std::vector<size_t> prefixSums{0};

        std::partial_sum(spaceSizes.begin(), spaceSizes.end(), std::back_inserter(prefixSums));

        Eigen::SparseMatrix<double, Eigen::RowMajor> matrix(samples.size(), prefixSums.back());
        Eigen::VectorXd targets(samples.size());

        for (size_t i = 0; i < samples.size(); ++i) {
            targets(i) = samples[i].Label;
            for (size_t j = 0; j < samples[i].Features.size(); ++j) {
                size_t featureIndex = samples[i].Features[j].second + prefixSums[j];
                matrix.insert(i, featureIndex);
            }
        }

        return std::make_pair(matrix, targets);
    }

private:
    TDataReader reader_;
};
