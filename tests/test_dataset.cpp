#include <gtest/gtest.h>
#include <dataset/dataset.h>
#include <dataset/csv_dataset.h>

static const std::string kDataDirectory = "../tests/test_data/";

TEST(Vector, NormalSize) {
    auto reader = std::make_shared<TDataReader>(kDataDirectory + "tsv_valid", EProblemType::kClassification, EDataFormat::kTsv);
    std::unique_ptr<IDataset> dataset = std::make_unique<TVectorDataset>(reader, 2);
    for (int i = 0; i < 10; ++i) {
        auto batch = dataset->GetBatch();
        ASSERT_EQ(batch.size(), 2);
        dataset->Next();
        ASSERT_FALSE(dataset->Empty());
    }
}

TEST(Vector, ExceedSize) {
    auto reader = std::make_shared<TDataReader>(kDataDirectory + "libsvm_valid", EProblemType::kRegression, EDataFormat::kLibSvm);
    std::unique_ptr<IDataset> dataset = std::make_unique<TVectorDataset>(reader, 10);
    TBatch batch;
    ASSERT_NO_THROW(batch = dataset->GetBatch());
    ASSERT_EQ(batch.size(), 2);
}

TEST(Disk, NormalSize) {
    auto reader = std::make_shared<TDataReader>(kDataDirectory + "tsv_valid", EProblemType::kClassification, EDataFormat::kTsv);
    std::unique_ptr<IDataset> dataset = std::make_unique<TDiskStorageDataset>(reader, 2);
    ASSERT_EQ(dataset->GetBatch().size(), 2);
    dataset->Next();
    ASSERT_EQ(dataset->GetBatch().size(), 1);
    ASSERT_TRUE(dataset->Empty());
}

TEST(Disk, ExceedSize) {
    auto reader = std::make_shared<TDataReader>(kDataDirectory + "libsvm_valid", EProblemType::kRegression, EDataFormat::kLibSvm);
    std::unique_ptr<IDataset> dataset = std::make_unique<TDiskStorageDataset>(reader, 3);
    ASSERT_EQ(dataset->GetBatch().size(), 2);
    ASSERT_TRUE(dataset->Empty());
}

TEST(Disk, CsvDataset) {
    TCSVDataset dataset(kDataDirectory + "csv_test", 1, 2, 3, 2);
    auto [nRows, nCols] = dataset.Size();
    ASSERT_EQ(nRows,3);
    ASSERT_EQ(nCols, 12);

    TDMatrix numericalFeatureMatrix(3, 4);
    numericalFeatureMatrix << 1, 0, 0, 1,
                              1, 0, 1, 0,
                              0, 1, 1, 0;

    TDMatrix s = TDMatrix::Zero(1, 12);
    for (int i = 0; i < 10; ++i) {
        TEigenBatch batch;
        dataset.GetBatch(batch);
        auto sample = TDMatrix(batch.first);
        ASSERT_EQ(sample.rows(), 1);
        ASSERT_EQ(sample.cols(), 12);

        for (int j = 0; j < 4; ++j) {
            ASSERT_EQ(sample(0, j), numericalFeatureMatrix(i % 3, j));
        }

        if (i < 3) {
            s += sample;
        }
    }

    for (int i = 4; i < 10; ++i) {
        ASSERT_EQ(s(0, i), 1);
    }
    ASSERT_TRUE((s(0, 10) == 1 && s(0, 11) == 2) || (s(0, 10) == 2 && s(0, 11) == 1));
}
