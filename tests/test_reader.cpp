#include <gtest/gtest.h>
#include <reader/reader.h>

static const std::string kDataDirectory = "../tests/test_data/";

TEST(Reader, ClfTsvValid) {
    TSpec spec{{1, EColumnType::kCategorical}};
    TDataReader reader(kDataDirectory + "tsv_valid",
                       EProblemType::kClassification, EDataFormat::kTsv);
    int i = 0;
    for (; reader.IsValid(); ++i) {
        ASSERT_EQ(reader.GetRow().Label, i % 2);
        reader.Next();
    }
    ASSERT_EQ(i, 3);
}

TEST(Reader, RegLibsvmValid) {
    TSpec spec{{1, EColumnType::kCategorical}};
    TDataReader reader(kDataDirectory + "libsvm_valid",
                       EProblemType::kRegression, EDataFormat::kLibSvm);
    int i = 0;
    for (; reader.IsValid(); ++i) {
        ASSERT_EQ(reader.GetRow().Label, (i + 1) * 2);
        reader.Next();
    }
    ASSERT_EQ(i, 2);
}



TEST(Reader, TsvBroken) {
    TDataReader reader(kDataDirectory + "tsv_broken",
            EProblemType::kClassification, EDataFormat::kTsv);
    ASSERT_ANY_THROW(reader.GetRow());
}
