#include <gtest/gtest.h>
#include <models/fm2.h>
#include <optimizers/sgd.h>
#include <iostream>
#include <optimizers/sgd.h>

TEST(FM, Criteo) {
//    TCSVDataset(const std::string& filename, char delimiter, size_t batchSize, size_t nNumericalFeatures,
//            size_t nCategoricalFeatures, size_t numericalDiscretizationFactor = 10, size_t minimalFrequency = 1)

    size_t batch_size = 32;
    double lr = 0.01;

    auto dataset = std::make_shared<TCSVDataset>("../data/dac_extra_small.txt", '\t', batch_size, 13, 26);
    TFM model(EProblemType::kClassification, dataset->Size().second, 16, 1);

    std::shared_ptr<IOptimizer> optimizer = std::make_shared<TSGDOptimizer>(lr);

//    shared_ptr<IOptimizer>& optimizer, size_t nEpochs = 3, size_t batchSize = 1024, double lr = 1e-3,
//            size_t savePeriod = 0, std::string checkpointPath = "model")

    model.Train(dataset, optimizer, 10, 1024, lr);

    TEigenBatch batch;

    dataset->GetBatch(batch);

    auto& [samples, targets] = batch;
    TDVector logits;

    model.Predict(samples, logits);
    auto loss = MSELoss(logits, targets);

    ASSERT_LE(loss, 10);
}
