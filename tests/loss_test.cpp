#include <gtest/gtest.h>
#include <loss/loss.h>

TEST(Loss, LogLoss) {
    Eigen::Vector4d targets(1., 0., 1., 1.);
    Eigen::Vector4d logits(0.5, 0.3, 0.7, 0.1);
    ASSERT_NEAR(BCELoss(logits, targets), 0.927, 0.001);
}