#include <gtest/gtest.h>
#include <models/logistic_regression.h>

double MeasureBatchAccuracy(std::shared_ptr<IModel> model, const TBatch& batch) {
    auto preds = model->Predict(batch);
    double good = 0;
    for (size_t i = 0; i < preds.size(); ++i) {
        good += (preds[i] == batch[i].Label);
    }
    return good / preds.size();
}

TEST(LogReg, Mnist) {
    auto reader = std::make_shared<TDataReader>("../data/mnist.txt", EProblemType::kClassification, EDataFormat::kTsv);
    auto dataset = std::make_shared<TVectorDataset>(reader, 100);
    auto model = std::make_shared<TLogisticRegression>(64, 0.1);
    model->Train(dataset);
    double avg_acc = 0;
    size_t batches = 100;
    for (size_t i = 0; i < batches; ++i) {
        avg_acc += MeasureBatchAccuracy(model, dataset->GetBatch());
    }
    avg_acc /= batches;
    ASSERT_GE(avg_acc, 0.95);
}
