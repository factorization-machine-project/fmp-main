#pragma once

#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>

#include <Eigen/Dense>
#include <Eigen/Sparse>

using TSMatrix = Eigen::SparseMatrix<double, Eigen::RowMajor>;
using TSVector = Eigen::SparseVector<double, Eigen::RowMajor>;
using TDMatrix = Eigen::MatrixXd;
using TDVector = Eigen::VectorXd;
using TIVector = Eigen::VectorXi;

#define Y_FAIL(a) throw std::runtime_error(a)
#define Y_ASSERT(a, b); if (a) { \
                        } else { \
                            Y_FAIL(b); \
                        }
