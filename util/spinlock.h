#include <atomic>
#include <thread>

class TSpinLock {
public:
    TSpinLock() = default;

    bool try_lock() {
        return !locked_.test_and_set(std::memory_order_acquire);
    }

    void lock() {
        while (!try_lock()) {
            std::this_thread::yield();
        }
    }

    void unlock() {
        locked_.clear(std::memory_order_release);
    }

private:
    std::atomic_flag locked_ = ATOMIC_FLAG_INIT;
};
