#pragma once

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <fstream>

namespace {
    using TTrip = Eigen::Triplet<int>;
}


template<class TMatrix>
void SerializeDense(const char* filename, const TMatrix& matrix){
    std::ofstream out(filename, std::ios::out | std::ios::binary | std::ios::trunc);
    typename TMatrix::Index rows = matrix.rows(), cols = matrix.cols();
    out.write((char*) (&rows), sizeof(typename TMatrix::Index));
    out.write((char*) (&cols), sizeof(typename TMatrix::Index));
    out.write((char*) matrix.data(), rows * cols * sizeof(typename TMatrix::Scalar));
    out.close();
}

template<class TMatrix>
void DeserializeDense(const char* filename, TMatrix& matrix){
    std::ifstream in(filename, std::ios::in | std::ios::binary);
    typename TMatrix::Index rows = 0, cols = 0;
    in.read((char*) (&rows),sizeof(typename TMatrix::Index));
    in.read((char*) (&cols),sizeof(typename TMatrix::Index));
    matrix.resize(rows, cols);
    in.read((char*) matrix.data(), rows * cols * sizeof(typename TMatrix::Scalar));
    in.close();
}

template<class TVector>
void SerializeVector(const char* filename, const TVector& vec){
    std::ofstream out(filename, std::ios::out | std::ios::binary | std::ios::trunc);
    typename TVector::Index size = vec.size();
    out.write((char*) (&size), sizeof(typename TVector::Index));
    out.write((char*) vec.data(), size * sizeof(typename TVector::Scalar));
    out.close();
}

template<class TVector>
void DeserializeVector(const char* filename, TVector& vec){
    std::ifstream in(filename, std::ios::in | std::ios::binary);
    typename TVector::Index size = 0;
    in.read((char*) (&size),sizeof(typename TVector::Index));
    vec.resize(size);
    in.read((char*) vec.data(), size * sizeof(typename TVector::Scalar));
    in.close();
}

template <typename T, int whatever, typename TInd>
void SerializeSparse(const char* filename, Eigen::SparseMatrix<T, whatever, TInd>& m) {
    std::vector<TTrip> res;
    int sz = m.nonZeros();
    m.makeCompressed();

    std::fstream writeFile;
    writeFile.open(filename, std::ios::binary | std::ios::out);

    if(writeFile.is_open())
    {
        TInd rows, cols, nnzs, outS, innS;
        rows = m.rows();
        cols = m.cols();
        nnzs = m.nonZeros();
        outS = m.outerSize();
        innS = m.innerSize();

        writeFile.write((const char *)&(rows), sizeof(TInd));
        writeFile.write((const char *)&(cols), sizeof(TInd));
        writeFile.write((const char *)&(nnzs), sizeof(TInd));
        writeFile.write((const char *)&(innS), sizeof(TInd));
        writeFile.write((const char *)&(outS), sizeof(TInd));

        writeFile.write((const char *)(m.valuePtr()),       sizeof(T) * m.nonZeros());
        writeFile.write((const char *)(m.outerIndexPtr()),  sizeof(TInd) * m.outerSize());
        writeFile.write((const char *)(m.innerIndexPtr()),  sizeof(TInd) * m.nonZeros());

        writeFile.close();
    }
}

template <typename T, int whatever, typename TInd>
void DeserializeSparse(const char* filename, Eigen::SparseMatrix<T, whatever, TInd>& m) {
    std::fstream readFile;
    readFile.open(filename, std::ios::binary | std::ios::in);
    if(readFile.is_open())
    {
        TInd rows, cols, nnz, inSz, outSz;
        readFile.read((char*)&rows, sizeof(TInd));
        readFile.read((char*)&cols, sizeof(TInd));
        readFile.read((char*)&nnz, sizeof(TInd));
        readFile.read((char*)&inSz, sizeof(TInd));
        readFile.read((char*)&outSz, sizeof(TInd));

        m.resize(rows, cols);
        m.makeCompressed();
        m.resizeNonZeros(nnz);

        readFile.read((char*)(m.valuePtr()), sizeof(T) * nnz);
        readFile.read((char*)(m.outerIndexPtr()), sizeof(TInd) * outSz);
        readFile.read((char*)(m.innerIndexPtr()), sizeof(TInd) * nnz);

        m.finalize();
        readFile.close();
    }
}
