## Factorization machine project
### Description:

Factorization Machines - machine learning models that were proposed by S. Rendle in [this article](https://www.csie.ntu.edu.tw/~b97053/paper/Rendle2010FM.pdf).
They use feature interactions which is efficient when the data is sparse. This library provides factorization machines that support classification and regression tasks.

### Supported data formats
##### CSV
Input file must be in csv format. The first column must contain the target followed by features separated by delimiter. First `n-num-features` are treated as numerical.
As factorization machines work with binary feature matrices, numerical features are encoded to binary vectors of dimension `discretization-factor`.

```
target1\delimiter\feature11\delimiter...feature1N\n
target2\delimiter\feature21\delimiter...feature2N\n
target3\delimiter\feature31\delimiter...feature3N\n
target4\delimiter\feature41\delimiter...feature4N\n
target5\delimiter\feature51\delimiter...feature5N\n
```

### Installation
```
git clone https://gitlab.com/factorization-machine-project/fmp-main.git
cd fmp-main
cmake -Bbuild CMakeLists.txt
make fmp # compiles binary
```

### Usage

`fmp train` - train model

`fmp eval` - predict values

Command line options:

* Generic options:
    * --model-type - Type of model to run. Possible values: logreg - logistic regression, fm - factorization machine. Default is fm.
    * --loss - Type of loss function to use: MSE or BCE. Possible values: mse, bce. Default is bce.
    * --batch-size - Size of batch to run optimizer on. Default value is 1024.
    * --model-checkpoint - Path to directory with serialized model. Default path is "model"
    * --problem-type - Classification or regression. Possible values - class, reg. Default is classification.
    * --dataset-state-file - Path to read stored parameters of dataset. During train parameters got loaded to "model-checkpoint/dataset_state"
* Options used for training run:
    * --train-file - name of file with training dataset.
    * --n-num-features - number of numerical features to read from train data file.
    * --n-cat-features - number of categorical features to read from train data file.
    * --min-feature-th - categorical values that appear less than min-feature-th times are excluded.
    * --epochs - number of epochs to run training for (how many times the dataset will be seen).
    * --embedding-size - Number of factors used in factorization machine model. Default is 16.
    * --learning-rate - learning rate of optimizer.
    * --delimiter - input file token delimiter. Default is ,.
    * --num-threads - number of threads to spawn for training. Default value is 1. Pass -1 to use all cores of your processor.
    * --checkpoint-period - dump current state of the model to disk every N batches.
    * --discretization-factor - number of buckets to group numerical features into. Default value is 10.
    * --optim - type of optimizer to use. Supported options are sgd, adam. Default value is sgd.
    * --beta1 - ADAM optimizer parameter. Default value is 0.9. Does not considered for SGD.
    * --beta2 - ADAM optimizer parameter. Default value is 0.99. Does not considered for SGD.
* Options used for evaluation run:
    * --test-file - name of file with evaluation dataset.
    * --output-file - name of file to store results of evaluation.
    * --model-path - path to trained model.
    * --has-target - read target values from dataset.


### Tests
```
cmake -H. -Bbuild
cd build
make tests
```

### Benchmarks
```
cmake -H. -Bbuild
cd build
make fm_benchmarks
```

### Authors
[@frammy](https://gitlab.com/frammy)
[@borisliskovets](https://gitlab.com/borisliskovets)
[@maximtim](https://gitlab.com/maximtim)
[@@anishenkokirill](https://gitlab.com/anishenkokirill)

### Comparison to libFM

libFM is a factorisation machine library proposed by S. Rendle. 
It is written in pure C++ and works fine using one thread only.
Here we present some comaprison benchmarks using Criteo, Avazu, Netflix datasets.
As one of advantages of fmp we consider the opportunity of learning keeping 
most of the training data directly on disk, so keep in mind that during training just 
some of training data is stored in RAM.

| Problem | libFM Time | fmp Time | libFM Loss | fmp Loss | libFM AUC | fmp AUC |
| ------- | --------- | ------- | ------- | ------- | ------- | ------- |
| Criteo(10M) | 469s  | 1357s   | 0.477   | 0.503   | 0.71    | 0.74    |
| Avazu(10M)  | 384s  | 1098s   | 0.438   | 0.319   | 0.76    | 0.8     |
| Netflix(~20M) | 166s| 165s    |    1.16 |    -    |    -    |    -    |
