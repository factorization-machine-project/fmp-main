#pragma once

#include <any>
#include <optional>
#include <vector>
#include <unordered_map>
#include <memory>

#include <config.h>
#include <util/definitions.h>


struct TCmdArg {
    std::string Name;
    EArgumentType Type;
    bool IsFlag;
    std::optional<std::string> DefaultValue;
};

struct TParsedArg {
    std::string Name;
    EArgumentType Type;
    std::any Value;
};

inline std::string stringToUpper(std::string s) {
    std::transform(s.begin(), s.end(), s.begin(), 
        [](unsigned char c){ return std::toupper(c); }
    );
    return s;
}

inline EDataFormat parseDataFormat(std::string str) {
    if (str.compare("tsv") == 0) return EDataFormat::kTsv;
    if (str.compare("libsvm") == 0) return EDataFormat::kLibSvm;
    return EDataFormat::kTsv;
}

inline EModelType parseModelType(std::string str) {
    if (str.compare("logreg") == 0) return EModelType::kLogReg;
    if (str.compare("fm") == 0) return EModelType::kFM;
    return EModelType::kLogReg;
}

inline EProblemType parseProblemType(std::string str) {
    if (str.compare("class") == 0) return EProblemType::kClassification;
    if (str.compare("reg") == 0) return EProblemType::kRegression;
    return EProblemType::kClassification;
}

inline EOptimizerType parseOptimizerType(std::string str) {
    if (str.compare("sgd") == 0) return EOptimizerType::kSGD;
    return EOptimizerType::kAdam;
}

inline EMode parseMode(const std::string& modeType) {
    if (modeType == "train") {
        return EMode::kTrain;
    }
    if (modeType == "eval") {
        return EMode::kEval;
    }
    Y_FAIL("Unhandled mode");
}


inline ELossType parseLossType(const std::string& lossName) {
    if (lossName == "mse") {
        return ELossType::kMSE;
    }
    if (lossName == "bce") {
        return ELossType::kBCE;
    }
    Y_FAIL("unhandled loss");
}


class TArgumentParser {
public:
    TArgumentParser() = default;

    TArgumentParser& RequiredArgument(const std::string& arg, EArgumentType type) {
        AddArgument("--" + arg, type);
        return *this;
    }

    TArgumentParser& OptionalArgument(const std::string& arg, EArgumentType type, const std::string& defaultValue = "") {
        AddArgument("--" + arg, type, false, defaultValue);
        return *this;
    }

    TArgumentParser& Flag(const std::string& arg) {
        AddArgument("--" + arg, EArgumentType::kBool, true);
        return *this;
    }

    TArgumentParser& AddSubparser(const std::string& mode) {
        Y_ASSERT(Children.find(mode) == Children.end(), "Duplicate mode: " + mode);

        Children.emplace(mode, std::make_shared<TArgumentParser>());

        return *Children[mode];
    }

    void AddArgument(const std::string& arg, EArgumentType type, bool isFlag = false, std::optional<std::string> defaultValue = std::nullopt) {
        Args.emplace_back(TCmdArg{arg, type, isFlag, std::move(defaultValue)});
    }

    void Parse(int argc, const char** argv) {
        for (const auto& arg : Args) {
            bool argExists = CmdOptionExists(argv, argv + argc, arg.Name);

            if (!argExists) {
                if (arg.DefaultValue != std::nullopt) {
                    TParsedArg parsedArg{
                        .Name = arg.Name,
                        .Type = arg.Type,
                        .Value = MakeArg(*arg.DefaultValue, arg.Type)
                    };
                    ParsedArgs.emplace(arg.Name.substr(2), std::move(parsedArg));
                } else if (arg.IsFlag) {
                    ParsedArgs.emplace(
                        arg.Name.substr(2),
                        TParsedArg{
                            .Name = arg.Name,
                            .Type = arg.Type,
                            .Value = false
                        }
                    );
                } else {
                    Y_FAIL("Missing required argument: " + arg.Name);
                }
            } else {
                if (arg.IsFlag) {
                    ParsedArgs.emplace(
                        arg.Name.substr(2),
                        TParsedArg{
                            .Name = arg.Name,
                            .Type = arg.Type,
                            .Value = true
                        }
                    );
                } else {
                    const auto* argValueStr = GetCmdOption(argv, argv + argc, arg.Name);

                    TParsedArg parsedArg{
                        .Name = arg.Name,
                        .Type = arg.Type,
                        .Value = MakeArg(argValueStr, arg.Type)
                    };

                    ParsedArgs.emplace(arg.Name.substr(2), std::move(parsedArg));
                }
            }
        }

        if (!Children.empty()) {
            const char* mode = argv[1];
            Y_ASSERT(Children.find(mode) != Children.end(), "Unhandled mode");
            auto& child = *Children[mode];
            child.Parse(argc - 1, argv + 1);

            ActiveMode = mode;

            return;
        }
    }

    std::any GetArg(const std::string& argName) const {
        if (ParsedArgs.find(argName) == ParsedArgs.end()) {
            if (!Children.empty()) {
                return Children.at(ActiveMode)->GetArg(argName);
            }
            Y_FAIL("Unknown arg: " + argName);
        }
        Y_ASSERT(ParsedArgs.find(argName) != ParsedArgs.end(), "Unknown arg: " + argName);
        return ParsedArgs.at(argName).Value;
    }

    const std::string GetActiveMode() const {
        return ActiveMode;
    }

private:
    const char* GetCmdOption(const char** begin, const char** end, const std::string& option) const {
        const char** itr = std::find(begin, end, option);
        if (itr != end && ++itr != end)
        {
            return *itr;
        }
        return 0;
    }

    bool CmdOptionExists(const char** begin, const char** end, const std::string& option) const {
        return std::find(begin, end, option) != end;
    }

    std::any MakeArg(const std::string& argValueStr, EArgumentType type) const {
        if (type == EArgumentType::kInt) {
            return std::any(std::stol(argValueStr));
        }
        if (type == EArgumentType::kFloat) {
            return std::any(std::stod(argValueStr));
        }
        if (type == EArgumentType::kString) {
            return std::any(argValueStr);
        }
        Y_FAIL("Unsupported argument type");
    }

    std::string ActiveMode;
    std::vector<TCmdArg> Args;
    std::unordered_map<std::string, TParsedArg> ParsedArgs;
    std::unordered_map<std::string, std::shared_ptr<TArgumentParser>> Children;
};
