#pragma once

#include <algorithm>
#include <random>
#include <memory>

#include <dataset/sample.h>
#include <reader/reader.h>

using TBatch = std::vector<TSample>;

class IDataset {
public:
    virtual void Next() = 0;

    virtual TBatch GetBatch() const = 0;

    virtual TSample GetSample() = 0;

    virtual bool Empty() = 0;

    virtual ~IDataset() = default;
};

class TVectorDataset : public IDataset { // at each iteration chooses batchSize random elements from dataset
public:
    TVectorDataset(std::shared_ptr<TDataReader> reader, size_t batchSize)
        : Reader(std::move(reader))
        , BatchSize(batchSize)
        , Generator{std::random_device{}()}
    {
        for (; Reader->IsValid(); Reader->Next()) {
            AllSamples.push_back(Reader->GetRow());
        }
        Next();
    }

    void Next() override {
        Batch.clear();
        std::sample(AllSamples.begin(), AllSamples.end(), std::back_inserter(Batch), BatchSize, Generator);
    }

    TBatch GetBatch() const override {
        return Batch;
    }

    TSample GetSample() override {
        throw std::runtime_error("unimplemented");
    }

    bool Empty() override {
        return false;
    }

private:
    std::shared_ptr<TDataReader> Reader;
    size_t BatchSize;
    std::mt19937 Generator;
    TBatch AllSamples;
    TBatch Batch;
};

class TDiskStorageDataset: public IDataset { // at each iteration reads batchSize elements from disk storage
public:
    TDiskStorageDataset(std::shared_ptr<TDataReader> reader, size_t batchSize)
        : Reader(std::move(reader))
        , BatchSize(batchSize)
    {
        Next();
    }

    void Next() override {
        Batch.clear();
        for (size_t i = 0; i < BatchSize && Reader->IsValid(); ++i) {
            Batch.push_back(Reader->GetRow());
            Reader->Next();
        }
    }

    TBatch GetBatch() const override {
        return Batch;
    }

    TSample GetSample() override {
        throw std::runtime_error("unimplemented");
    }

    bool Empty() override {
        return !Reader->IsValid();
    }

private:
    std::shared_ptr<TDataReader> Reader;
    size_t BatchSize;
    TBatch Batch;
};

