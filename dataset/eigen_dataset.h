#pragma once

#include <algorithm>
#include <random>
#include <memory>

#include <dataset/sample.h>
#include <reader/csv_reader.h>
#include <util/definitions.h>

namespace {
    using TEigenBatch = std::pair<TSMatrix, TDVector>;
}

//class IDataset {
//public:
//    virtual void Reset() = 0;
//
//    virtual TEigenBatch GetBatch(size_t i) const = 0;
//
//    virtual size_t Size() const = 0;
//};

class TDataset {
public:
    TDataset(const std::string& filename, std::shared_ptr<TCsvReader> reader, size_t batchSize, size_t nLinesToRead = 0, bool shuffle = true, bool initialize = false)
        : FileName(filename)
        , Reader(std::move(reader))
        , BatchSize(batchSize)
        , NLinesToRead(nLinesToRead)
        , Shuffle(shuffle)
        , Initialized(false)
    {
        if (initialize) {
            Reset();
        }
    }

    void Reset() {
        if (!Initialized) {
            Reader->Read(FileName, X, Y, NLinesToRead);

            Initialized = true;
        }

        if (Shuffle) {
            Indices.resize(X.rows(), 0);
            for (size_t i = 0; i < X.rows(); ++i) {
                Indices[i] = i;
            }

            std::random_device rd;
            std::mt19937 g(rd());
            std::shuffle(Indices.begin(), Indices.end(), g);
        }
    }

    TEigenBatch GetBatch(size_t i) const {
        auto sz = Size();
        if (i * BatchSize >= sz) {
            Y_FAIL("attempt to get batch by index greater than total dataset size");
        }
        auto batchSize = std::min(sz - i, BatchSize);

        if (!Shuffle) {
            TSMatrix samples = X.middleRows(i, batchSize);
            TDVector targets = Y.middleRows(i, batchSize);
            return std::make_pair(samples, targets);
        }

        TEigenBatch batch;

        auto& batchX = batch.first;
        auto& batchY = batch.second;

        batchX.resize(batchSize, NFeatures());
        batchY.resize(batchSize);

        for (size_t j = i; j < i + batchSize; ++j) {
            auto ri = Indices[j];
            const TSVector& r = X.row(ri);

            for (TSVector::InnerIterator it(r); it; ++it) {
                batchX.insert(j - i, it.col()) = r.coeff(it.col());
            }

            batchY(j - i) = Y(ri);
        }
        return batch;
    }

    size_t Size() const {
        return X.rows();
    }

    size_t NFeatures() const {
        return X.cols();
    }

    const TSMatrix GetSamples() const {
        return X;
    }

    const TDVector GetTargets() const {
        return Y;
    }

private:
    std::string FileName;
    std::shared_ptr<TCsvReader> Reader;
    size_t BatchSize;
    size_t NLinesToRead;
    TSMatrix X;
    TDVector Y;
    std::vector<size_t> Indices;
    bool Shuffle;
    bool Initialized;
};
