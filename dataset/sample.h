#pragma once

#include <vector>

using TIndex = uint64_t;
using TValue = double;

struct TSample {
    double Label;
    std::vector<std::pair<TIndex, TValue>> Features;
};
