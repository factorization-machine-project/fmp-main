#pragma once

#include <MiniCSV/minicsv.h>
#include <util/definitions.h>

#include <algorithm>

using TEigenBatch = std::pair<TSMatrix, TDVector>;

class TCSVDataset {
public:
    TCSVDataset() = default;

    TCSVDataset(const std::string& filename, size_t batchSize, size_t nNumericalFeatures,
            size_t nCategoricalFeatures, size_t numericalDiscretizationFactor = 10, size_t minimalFrequency = 1, char delimiter=',')
        : Filename(filename)
        , Delimiter(delimiter)
        , BatchSize(batchSize)
        , NNumericalFeatures(nNumericalFeatures)
        , NCategoricalFeatures(nCategoricalFeatures)
        , DiscretizationFactor(numericalDiscretizationFactor)
        , MinimalFrequency(minimalFrequency)
        , NumericalThresholds(nNumericalFeatures)
        , CategoricalIds(nCategoricalFeatures)
        , CategoricalGlobalIndices(nCategoricalFeatures) {
        Input.set_delimiter(Delimiter, Input.get_unescape_str());
        Reset();
        InitState();
    }

    TCSVDataset(std::string filename, size_t batchSize, const std::string& statePath, char delimiter=',')
        : Filename(std::move(filename))
        , Delimiter(delimiter)
        , BatchSize(batchSize) {
        Input.set_delimiter(delimiter, Input.get_unescape_str());
        Reset();
        LoadState(statePath);
    }

    void HasTarget(bool has) {
        Train = has;
    }

    void SetFile(const std::string& filename) {
        Filename = filename;
        Reset();
        CountRows();
    }

    void SetBatchSize(size_t batchSize) {
        BatchSize = batchSize;
    }

    void GetBatch(TEigenBatch &batch) {
        batch.first.resize(BatchSize, NCols);
        batch.first.setZero();

        batch.second.resize(BatchSize);
        batch.second.setZero();

        for (size_t rowIndex = 0; rowIndex < BatchSize; ++rowIndex) {
            while (!Input.read_line()) {
                Reset();
            }

            if (Train) {
                double targetValue = 0.L;
                Input >> targetValue;
                batch.second(rowIndex) = targetValue;
            }

            for (size_t featureIndex = 0; featureIndex < NNumericalFeatures; ++featureIndex) {
                double featureValue = 0.L;
                Input >> featureValue;
                EncodeNumerical(rowIndex, featureIndex, featureValue, batch);
            }

            for (size_t featureIndex = 0; featureIndex < NCategoricalFeatures; ++featureIndex) {
                std::string featureValue;
                Input >> featureValue;
                EncodeCategorical(rowIndex, featureIndex, featureValue, batch);
            }
        }
    }

    std::pair<size_t, size_t> Size() const {
        return std::make_pair(NRows, NCols);
    }

    void SaveState(const std::string &path) const {
        std::ofstream out(path);
        if (!out.is_open()) {
            Y_FAIL("can't write to file: " + path);
        }

        out << NRows << ' ' << NCols << '\n';
        out << NNumericalFeatures << ' ' << NCategoricalFeatures << '\n';
        out << DiscretizationFactor << '\n';

        for (const auto& thresholds : NumericalThresholds) {
            for (const auto& threshold : thresholds) {
                out << threshold << ' ';
            }
            out << '\n';
        }

        for (const auto& ids : CategoricalIds) {
            out << ids.size() << '\n';
            for (const auto& [value, id] : ids) {
                out << value << ' ' << id << '\n';
            }
        }

        for (const auto& index : CategoricalGlobalIndices) {
            out << index << ' ';
        }
    };

    void LoadState(const std::string &path) {
        std::ifstream in(path);
        if (!in.is_open()) {
            Y_FAIL("can't read file: " + path);
        }

        in >> NRows >> NCols >> NNumericalFeatures >> NCategoricalFeatures >> DiscretizationFactor;

        NumericalThresholds.assign(NNumericalFeatures, std::vector<double>(DiscretizationFactor));
        for (auto& thresholds : NumericalThresholds) {
            for (auto& threshold : thresholds) {
                in >> threshold;
            }
        }

        CategoricalIds.assign(NCategoricalFeatures, TValuesIds());
        for (auto& ids : CategoricalIds) {
            size_t uniqueValues = 0;
            in >> uniqueValues;
            ids.reserve(uniqueValues);
            for (size_t i = 0; i < uniqueValues; ++i) {
                std::string value;
                size_t id;
                in >> value >> id;
                ids[value] = id;
            }
        }

        CategoricalGlobalIndices.assign(NCategoricalFeatures, 0);
        for (auto& index : CategoricalGlobalIndices) {
            in >> index;
        }
    };

private:

    void Reset() {
        Input = mini::csv::ifstream(Filename);
        Input.set_delimiter(Delimiter, Input.get_unescape_str());
        Input.set_ignore_errors(true);
        if (!Input.is_open()) {
            Y_FAIL("can't open file: " + Filename);
        }
    }

    void CountRows() {
        for (NRows = 0; Input.read_line(); ++NRows) {
        }
    }

    void InitState() {
        std::cout << "Initializing dataset...\n";
        std::vector<std::pair<double, double>> numericalBounds(NNumericalFeatures,
                std::make_pair(std::numeric_limits<double>::infinity(), -std::numeric_limits<double>::infinity()));
        std::vector<TValuesFrequencies> categoricalFrequencies(NCategoricalFeatures);

        while (Input.read_line()) {
            ++NRows;

            double targetValue = 0.L;
            Input >> targetValue;

            for (size_t i = 0; i < NNumericalFeatures; ++i) {
                double featureValue = 0.L;
                Input >> featureValue;

                numericalBounds[i].first = std::min(numericalBounds[i].first, featureValue);
                numericalBounds[i].second = std::max(numericalBounds[i].second, featureValue);
            }

            for (size_t i = 0; i < NCategoricalFeatures; ++i) {
                std::string featureValue;
                Input >> featureValue;
                ++categoricalFrequencies[i][featureValue];
            }

            if (NRows % 1000 == 0) {
                std::cout << "\033[2K\r" << std::flush << "Samples processed: " << NRows << std::flush;
            }
        }
        std::cout << "\033[2K\r" << std::flush << "Samples processed: " << NRows << std::endl;

        for (size_t i = 0; i < NNumericalFeatures; ++i) {
            const auto& [min, max] = numericalBounds[i];
            NumericalThresholds[i].resize(DiscretizationFactor);
            double step = (max - min) / DiscretizationFactor;
            double threshold = max;
            for (size_t j = 0; j < DiscretizationFactor; ++j) {
                NumericalThresholds[i][DiscretizationFactor - 1 - j] = threshold;
                threshold -= step;
            }
        }

        std::vector<size_t> nextId(NCategoricalFeatures);
        for (size_t i = 0; i < categoricalFrequencies.size(); ++i) {
            for (const auto& [featureValue, frequency] : categoricalFrequencies[i]) {
                if (frequency >= MinimalFrequency) {
                    CategoricalIds[i][featureValue] = nextId[i]++;
                }
            }

            if (i == 0) {
                CategoricalGlobalIndices[i] = NNumericalFeatures * DiscretizationFactor;
            } else {
                CategoricalGlobalIndices[i] = CategoricalGlobalIndices[i - 1] + CategoricalIds[i - 1].size();
            }
        }

        NCols = CategoricalGlobalIndices[NCategoricalFeatures - 1] + CategoricalIds[NCategoricalFeatures - 1].size();

        std::cout << "Features count: " << NCols << std::endl;
    }

    void EncodeNumerical(size_t rowIndex, size_t featureIndex, double featureValue, TEigenBatch& batch) {
        size_t id = std::lower_bound(NumericalThresholds[featureIndex].begin(),
                                     NumericalThresholds[featureIndex].end(), featureValue)
                                                      - NumericalThresholds[featureIndex].begin();
        featureIndex = featureIndex * DiscretizationFactor + id;
        batch.first.insert(rowIndex, featureIndex) = 1;
    }

    void EncodeCategorical(size_t rowIndex, size_t featureIndex, const std::string& featureValue, TEigenBatch& batch) {
        size_t id = CategoricalIds[featureIndex][featureValue];
        featureIndex = CategoricalGlobalIndices[featureIndex] + id;
        batch.first.insert(rowIndex, featureIndex) = 1;
    }

private:
    std::string Filename;
    mini::csv::ifstream Input;

    size_t BatchSize = 0;
    size_t NNumericalFeatures = 0;
    size_t NCategoricalFeatures = 0;

    size_t DiscretizationFactor = 1;
    size_t MinimalFrequency = 1;

    char Delimiter;

    std::vector<std::vector<double>> NumericalThresholds;

    using TValuesFrequencies = std::unordered_map<std::string, size_t>;
    using TValuesIds = std::unordered_map<std::string, size_t>;
    std::vector<TValuesIds> CategoricalIds;
    std::vector<size_t> CategoricalGlobalIndices;

    // rows count in entire dataset
    size_t NRows = 0;
    size_t NCols = 0;

    bool Train = true;
};
